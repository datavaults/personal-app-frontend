# Personal App Frontend

The personal app frontend application

## Installation

1. Make sure you have node version 14

```bash
# install version 14
$ nvm install 14

# use version 14
$ nvm use 14

# make it the default
$ nvm alias default 14
```

2. Install dependencies

```bash
$ yarn
```

3. Create .env

```bash
$ cp env.sample .env
```

## Running the app

```bash
# development
$ yarn dev
```

```bash
# needed to connect with facebook, etc.
$ yarn dev --https
```

## Recipes

### Showing a toast

To show a toast import `useToaster()` composable and call `.show()`.

```vue
<template>
  <div></div>
</template>
<script lang="ts">
import { defineComponent } from "vue";
import { useToaster } from "@/composables";

export default defineComponent({
  name: "Dashboard",
  setup() {
    const toaster = useToaster();
    toaster.show("your message"); // this will show an info message
    toaster.show("your message", "info"); // this will show an info message
    toaster.show("your message", "warning"); // this will show a warning message
    toaster.show("your message", "error", "your title"); // this will show an error message with a title
    toaster.show("your message", "error", "your title", 10000); // this will show an error message with a title and custom hide timeout (default 5000 milliseconds)
  },
});
</script>
```
