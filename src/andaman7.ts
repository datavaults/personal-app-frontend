import axios from "axios";
import sha256 from "crypto-js/sha256";
import { v4 as uuidv4 } from "uuid";
import { getEnv } from "./envs";

const andaman7URL = getEnv("VITE_ANDAMAN7_URL");
const andaman7ApiKey = getEnv("VITE_ANDAMAN7_API_KEY");
const andaman7User = "/users/me";
const credentials = (username: any, password: any) => {
  const userCredentials = username + ":" + sha256(password);
  const userHashedCredentials = btoa(userCredentials);
  return userHashedCredentials;
};

export default {
  confirmUser: async (username: any, password: any) => {
    const user = credentials(username, password);
    return await axios.get(andaman7URL + andaman7User, {
      headers: {
        accept: "application/json",
        "api-key": andaman7ApiKey,
        Authorization: "Basic " + user,
      },
    });
  },
  convertPassword: (password: any) => {
    return sha256(password);
  },
  createDevice: async (username: any, password: any, deviceProperty: any) => {
    const user = credentials(username, password);
    const device = {
      id: uuidv4(),
      name: "datavaults",
      active: true,
      properties: deviceProperty
    };
    return await axios.post(andaman7URL + andaman7User + "/devices", device, {
      headers: {
        accept: "application/json",
        "api-key": andaman7ApiKey,
        Authorization: "Basic " + user,
      },
    });
  },
};
