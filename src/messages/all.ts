const locales: {
  code: string;
  label: string;
  flagCode: string;
}[] = [
  {
    code: "en",
    label: "English",
    flagCode: "gb",
  },
  {
    code: "gr",
    label: "Greek",
    flagCode: "gr",
  },
  {
    code: "it",
    label: "Italian",
    flagCode: "it",
  },
  {
    code: "es",
    label: "Spanish",
    flagCode: "es",
  },
  {
    code: "fr",
    label: "French",
    flagCode: "fr",
  },
];

export function useLocale() {
  const defaultLocale: { code: string; label: string; flagCode: string } =
    locales[0];
  const find = (code: string) => {
    return locales.find(
      (locale: { code: string; label: string; flagCode: string }) =>
        locale.code === code
    );
  };
  const localeCodes = locales.map((locale: { code: string }) => locale.code);

  return { locales, localeCodes, defaultLocale, find };
}
