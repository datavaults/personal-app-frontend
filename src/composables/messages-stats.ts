interface Questionnaire {
  month: string;
  total: number;
  respondAccepted: number;
  respondDeclined: number;
  totalPoint: number;
}

import { MessagesStats } from "@/api";
import { ref } from "vue";

export function useMessagesStats() {
  const questionnaires = ref<object[]>([]);
  const monthlyQResponded = ref<number[]>([]);
  const monthlyQReceived = ref<number[]>([]);
  const monthlyQPoints = ref<number[]>([]);
  const totalPoints = ref<any>();
  const totalSold = ref<any>();
  const loading = ref<boolean>(false);

  const refresh = (year: string) => {
    loading.value = true;
    MessagesStats.getQuestionnaires(year)
      .then((res: any) => {
        questionnaires.value = res.data as any;
        monthlyQResponded.value = res.data.map(
          (item: Questionnaire) => Number(item.respondAccepted) + Number(item.respondDeclined)
        );
        monthlyQReceived.value = res.data.map((item: Questionnaire) => Number(item.total));
        monthlyQPoints.value = res.data.map((item: Questionnaire) => Number(item.totalPoint));
      })
      .finally(() => (loading.value = false));
  };

  const points = () => {
    MessagesStats.getSoldQuestionnaires()
      .then((res: any) => {
        totalPoints.value = res.data.totalPoints;
        totalSold.value = res.data.totalSold;
      })
      .finally(() => (loading.value = false));
  };

  return { refresh, monthlyQResponded, monthlyQReceived, monthlyQPoints, points, totalPoints, totalSold, loading };
}
