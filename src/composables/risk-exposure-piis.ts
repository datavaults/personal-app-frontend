import { RiskManagement } from "@/api";
import { ref } from "vue";

export function useRiskExposurePiis() {
  const piis = ref([]);

  RiskManagement.getPiis().then((res: any) => {
    if (res.status >= 200 && res.status < 400) {
      piis.value = res.data;
    } else piis.value = [];
  });

  return { piis };
}
