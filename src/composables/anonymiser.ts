import { ref } from "vue";
import { Anonymiser as AnonymiserAPI } from "@/api";

export function useAnonymiser() {
  const existingPseudoIds = ref<{ label: string; value: string }[]>([]);

  AnonymiserAPI.getPseudoIds().then((res: any) => {
    existingPseudoIds.value = res.data;
  });

  return { existingPseudoIds };
}
