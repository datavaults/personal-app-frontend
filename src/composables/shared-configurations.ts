import { ref } from "vue";
import { SharingConfiguration } from "@/api";

export function useSharedConfigurations() {
  const configurations = ref<any>([]);
  const loading = ref<boolean>(false);
  const configuration = ref<any>(null);

  const refresh = () => {
    loading.value = true;
    SharingConfiguration.all().then((res: any) => {
        configurations.value = res.data;
      loading.value = false;
    });
  };

  const deleteConfiguration = async (id: any) => {
    await SharingConfiguration.delete(id)
    refresh();
  }

  const getConfiguration = async (id: any) => {
    SharingConfiguration.get(id).then( async (res: any) => {
        return configuration.value = res.data;
    }).finally(() => (loading.value = false));
  };
  
  refresh();

  return { configurations, loading, refresh, deleteConfiguration, getConfiguration, configuration };
}
