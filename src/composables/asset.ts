import * as R from "ramda";
import { computed, Ref, ref, watch } from "vue";
import { Asset } from "@/interfaces";
import { DataFetcher, DownloadData } from "@/api";
import jsonld from "jsonld";
import { ColumnType, NAMESPACE } from "@/constants";
import { useDownloadable } from "./downloadable";

const DATASET_IRI = "http://www.w3.org/ns/dcat#Dataset";
const KEY_COLUMN_IRI = `${NAMESPACE}/dv#hasKeyColumn`;
const VALUE_COLUMN_IRI = `${NAMESPACE}/dv#hasValueColumn`;
const COLUMN_HEADER_IRI = `${NAMESPACE}/dv#columnHeader`;
const COLUMN_TYPE_IRI = `${NAMESPACE}/dv#columnType`;

function calculateColumns(metadata: any): Promise<any | null> {
  return new Promise(async (resolve) => {
    const flat: any = await jsonld.flatten(metadata);
    const datasetSchema = flat.find((entry: any) => {
      return (
        entry["@type"] === DATASET_IRI ||
        (R.is(Array, entry["@type"]) && entry["@type"].includes(DATASET_IRI))
      );
    });
    if (
      datasetSchema &&
      (datasetSchema[KEY_COLUMN_IRI] || datasetSchema[VALUE_COLUMN_IRI])
    ) {
      let allColumnEntries: any[] = [];
      if (datasetSchema[KEY_COLUMN_IRI])
        allColumnEntries = [
          ...allColumnEntries,
          ...datasetSchema[KEY_COLUMN_IRI],
        ];
      if (datasetSchema[VALUE_COLUMN_IRI])
        allColumnEntries = [
          ...allColumnEntries,
          ...datasetSchema[VALUE_COLUMN_IRI],
        ];
      const columns = allColumnEntries.reduce((acc: any, columnKeyObj: any) => {
        const columnKey = columnKeyObj["@id"];
        const col: any = R.find(R.propEq("@id", columnKey))(flat);
        const columnName = col[COLUMN_HEADER_IRI][0]["@value"];
        const columnTypeIri = col[COLUMN_TYPE_IRI][0]["@id"];
        acc[columnName] = ColumnType.findByIri(columnTypeIri);
        return acc;
      }, {});
      resolve(columns);
    }

    resolve(null);
  });
}

export function useAsset(assetId: Ref<number | null>) {
  const { download } = useDownloadable();
  const asset = ref<Asset | null>(null);
  const loading = ref<boolean>(false);
  const columns = ref<any>({});
  let sampleData = ref<any>([]);

  const getAsset = () => {
    if (assetId.value) {
      loading.value = true;
      DataFetcher.getAsset(assetId.value)
        .then(async ({ data }) => {
          asset.value = data;
          if(asset.value?.sourceType === "MOBILEAPP"){
            const filteredData = asset.value.data.filter((elem: any) => !R.isEmpty(elem) && !R.isNil(elem));
            asset.value.data = filteredData;
          }
          sampleData.value = asset.value ? asset.value.data : []
        })
        .finally(() => (loading.value = false));
    } else {
      asset.value = null;
    }
  };

  const fields = computed(() =>{
    if(sampleData.value.length > 0 &&
    !R.isNil(columns.value) &&
    !R.isEmpty(columns.value)){
     return Object.keys(sampleData.value[0]).map((k: string) => {
        return {
          key: k,
          label: k,
          type: R.has("anonymisationType", columns.value[k])
            ? columns.value[k].anonymisationType
            : "string",
        };
      })
    } else if(sampleData.value.length > 0){
      return Object.keys(sampleData.value[0]).map((k: string) => {
        return {
          key: k,
          label: k,
          type: "string"
        };
      })
    } else {
      return [];
    }
  }
  );

  const metadata = computed(() => asset.value?.metadata ? asset.value?.metadata : []);

  const downloadData = async () => {
    if (asset.value && assetId.value && asset.value.sourceType != 'PRATO' ) {
      DataFetcher.downloadData(assetId.value).then((res: any) => {
        download(String(asset.value?.name), [
          JSON.stringify(res.data, null, "\t"),
        ]);
      });
    } else {
      DownloadData.downloadSpecificData([assetId.value as any]).then((res: any) => {
        download(asset.value?.name as any, [res.data], ".pdf", true, res.headers["content-type"]);
      });
    }
      
  };

  const downloadMetadata = () => {
    if (asset.value && assetId.value)
      DataFetcher.downloadMetadata(assetId.value).then((res: any) => {
        download(String(asset.value?.name), [
          JSON.stringify(res.data, null, "\t"),
        ]);
      });
  };

  const downloadDataSchema = () => {
    if (asset.value && assetId.value)
      DataFetcher.downloadDataSchema(assetId.value).then((res: any) => {
        download(String(asset.value?.name), [
          JSON.stringify(res.data, null, "\t"),
        ]);
      });
  };

  watch(
    () => assetId.value,
    () => {
      getAsset();
    }
  );

  watch(
    () => metadata.value,
    async (updatedMetadata: any) => {
      if (updatedMetadata) {
        columns.value = await calculateColumns(updatedMetadata);
      } else {
        columns.value = [];
      }
    }
  );

  getAsset();

  return {
    asset,
    columns,
    fields,
    sampleData,
    loading,
    metadata,
    downloadData,
    downloadMetadata,
    downloadDataSchema,
  };
}
