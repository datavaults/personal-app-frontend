import { Ref } from "vue";

export function useDownloadable() {
  const download = (
    name: string,
    data: any,
    extension: string = ".json",
    appendDate: boolean = true,
    type: string = "text/plain;charset=utf-8"
  ) => {
    const blob = new Blob(data, {
      type: type,
    });
    const link = URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.download = `${name}@${appendDate ? Date.now() : ""}${extension}`;
    a.href = link;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };

  return { download };
}
