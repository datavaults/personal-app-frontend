import { computed, ref } from "vue";
import * as R from "ramda";
import { LiveMessage, Message } from "@/interfaces";
import { Messages as MessagesAPI } from "@/api";
import { MessageTypeWrapper, RequestResponseType } from "@/constants";
import { useSSE, useToaster } from "@/composables";
import i18n from "../main";

const messages = ref<Message[]>([]);
const isFirstTime = ref<Boolean>(true);
const isLoading = ref<Boolean>(false);
const toaster = useToaster();

export function useMessages() {
  const unreadMessages = computed(() =>
    messages.value.filter((message: Message) => R.isNil(message.readAt))
  );

  const requestMessages = computed(() =>
    messages.value.filter((message: Message) => {
      const messageType = MessageTypeWrapper.find(message.messageType);
      return !R.isNil(messageType) && messageType.isRequest();
    })
  );

  const pendingRequests = computed(() =>
    requestMessages.value.filter(
      (message: Message) => message.response === RequestResponseType.Pending
    )
  );

  const refresh = () => {
    if (!isLoading.value) {
      isLoading.value = true;
      MessagesAPI.getAll().then((res: any) => {
        messages.value = res.data;
        isLoading.value = false;
        isFirstTime.value = false;
      });
    }
  };

  const deleteMessage = async (messageId: number) => {
    await MessagesAPI.delete(messageId);
    refresh();
  };

  if (isFirstTime.value) {
    refresh();
  }

  useSSE(`/api/message/sse`, async (message: LiveMessage) => {
    const data = message.payload;
    const messageType = MessageTypeWrapper.find(data.messageType);

    if (messageType)
      toaster.show(
        messageType.getNotification(message),
        "info",
        i18n.global.t('messages.newMessage')
      );
    else {
      console.log("Message type not found");
    }
    refresh();
  });

  return {
    messages,
    unreadMessages,
    requestMessages,
    pendingRequests,
    refresh,
    deleteMessage,
    loading: isLoading,
  };
}
