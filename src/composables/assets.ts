import { ref } from "vue";
import { Asset } from "@/interfaces";
import { DataFetcher } from "@/api";

export function useAssets() {
  const assets = ref<Asset[]>([]);
  const loading = ref<boolean>(false);

  const refresh = () => {
    loading.value = true;
    DataFetcher.getAssets().then(({ data }) => {
      assets.value = data;
      loading.value = false;
    });
  };

  const deleteAsset = (id: number): Promise<void> => {
    return new Promise((resolve, reject) => {
      DataFetcher.deleteAsset(id)
        .then(() => {
          refresh();
          resolve();
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  };

  refresh();

  return { assets, loading, deleteAsset, refresh };
}
