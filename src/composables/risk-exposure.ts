import { RiskManagement } from "@/api";
import { RiskScoreConfiguration, RiskScoreMapping, ShareConfiguration } from "@/interfaces";
import * as R from "ramda";
import { computed, ref, Ref, watch } from "vue";

export function useRiskExposure(configuration: Ref<ShareConfiguration>) {
  const numericExposure: Ref<number | undefined> = ref<number | undefined>();

  const riskScoreConfiguration: Ref<RiskScoreConfiguration> = computed(() => {
    return {
      assetId: configuration.value.assetUUID,
      assetName: configuration.value.name,
      useTmp: configuration.value.configuration.useTPM,
      usePersona: configuration.value.configuration.persona,
      useEncryption: configuration.value.configuration.encryption,
      collection: configuration.value.fieldMapping.filter((mapping: RiskScoreMapping) => !R.isNil(mapping.piiId)),
    };
  });

  watch(
    () => riskScoreConfiguration.value,
    (config: any) => {
      RiskManagement.getPrivacyScore(config).then((res: any) => {
        if (res.status >= 200 && res.status < 400) {
          numericExposure.value = res.data.score;
        } else numericExposure.value = undefined;
      });
    },
    { immediate: true, deep: true }
  );

  return { numericExposure };
}
