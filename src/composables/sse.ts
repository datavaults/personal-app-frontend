import { SseQueue } from "@/constants";
import * as R from "ramda";
import { SseMessage } from "@/interfaces";

let es: EventSource | null;

export function useSSE(
  sseUrl: string,
  onNewMessage: Function,
  onError: Function = async (event: any) => {
    if (event.readyState === EventSource.CLOSED) {
      console.log("Event was closed");
      console.log(EventSource);
    }
  }
) {
  const destroyEventSource = () => {
    if (es) {
      es.close();
      es = null;
    }
  };

  const initialiseEventSource = (url: string, queue: SseQueue) => {
    if (es) destroyEventSource();
    es = new EventSource(url, { withCredentials: true });

    es.addEventListener("message", (event: MessageEvent) => {
      if (R.isNil(event) || R.isNil(event.data)) return;
      const { message, queue: messageQueue } = JSON.parse(
        event.data
      ) as SseMessage;
      if (messageQueue === queue) {
        onNewMessage(message);
      }
    });

    es.addEventListener("error", (event: any) => {
      onError(event);
    });
  };

  initialiseEventSource(sseUrl, SseQueue.Message);
}
