import { TPMApi } from "@/api";
import { ref } from "vue";
import { useToaster } from "@/composables";
import { useRoute } from "vue-router";
import i18n from "../main";

const toaster = useToaster();

export const useTPMCheck = () => {
  const activateTpm = ref<boolean>(false);
  const tpmPseudonyms = ref<any>([]);



  
  const route = useRoute();

  const isOnAnonymisation = ref(route.path === "/share/anonymisation");

  const joinTPMDAA = async () => {
    let joinTPM = await TPMApi.joinTPMDAA();
    if (joinTPM.status != 200) {
      return;
    }
    if (activateTpm && joinTPM.status != 200) {
      if (isOnAnonymisation.value) toaster.show(i18n.global.t('tpm.errorCommunicating'), "error");
      return;
    }
    activateTpm.value = true;
    if (isOnAnonymisation.value) toaster.show(i18n.global.t('tpm.tpmReady'), "info");
    readPseudonymsFromTPM();
    return activateTpm;
  };

  const readPseudonymsFromTPM = async () => {
    let pseudo = await TPMApi.readPseudonymsFromTPM();
    if (activateTpm.value === true) {
      if (pseudo.error) {
        if (isOnAnonymisation.value) toaster.show(i18n.global.t('tpm.errorCommunicating'), "error", pseudo.error);
        return;
      }
      for (var i = 0; i < pseudo.length; i++) {
        tpmPseudonyms.value.push({
          label: pseudo[i],
          value: pseudo[i],
          description: i18n.global.t('tpm.pseudonymNumber') + i,
        });
      }
      if (isOnAnonymisation.value) toaster.show(i18n.global.t('tpm.pseudonymsReceived'), "info");
    }
  };

  const createTPMPseudonym = async () => {
    let createPseudo = await TPMApi.createTPMPseudonym();
    if (isOnAnonymisation.value) toaster.show(i18n.global.t('tpm.pseudonymCreated'), "info");
    tpmPseudonyms.value.push({
      label: createPseudo.label,
      value: createPseudo.value,
      description: i18n.global.t('tpm.newPseudonym'),
    });
  };

  return {
    joinTPMDAA,
    createTPMPseudonym,
    readPseudonymsFromTPM,
    activateTpm,
    tpmPseudonyms,
  };
};
