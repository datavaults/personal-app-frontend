import * as R from "ramda";
import { computed } from "vue";
import store from "@/store";
import { Auth, User } from "@/api";
import Keycloak from "keycloak-js";
import router from "../router";
import { useToaster } from "@/composables/toaster";
import { getEnv } from "./../envs";
import { useDemonstrators } from "./demonstrator";

let keycloak: any | null = null;
if (
  !R.isNil(getEnv("VITE_KEYCLOAK_URL")) &&
  !R.isEmpty(getEnv("VITE_KEYCLOAK_URL")) &&
  !R.isNil(getEnv("VITE_KEYCLOAK_REALM")) &&
  !R.isEmpty(getEnv("VITE_KEYCLOAK_REALM")) &&
  !R.isNil(getEnv("VITE_KEYCLOAK_CLIENT_ID")) &&
  !R.isEmpty(getEnv("VITE_KEYCLOAK_CLIENT_ID"))
) {
  // @ts-ignore
  keycloak = Keycloak<"native">({
    url: getEnv("VITE_KEYCLOAK_URL"),
    realm: getEnv("VITE_KEYCLOAK_REALM") || "",
    clientId: getEnv("VITE_KEYCLOAK_CLIENT_ID") || "",
  });
}

const toaster = useToaster();
const { shouldGoToTerms } = useDemonstrators();

export function useAuthentication() {
  const isEnabled = computed(() => !R.isNil(keycloak));
  const isAuthenticated = computed(() => {
    return keycloak.authenticated && !keycloak.isTokenExpired();
  });

  const isNotVerified = computed(() => store.state.user && !store.state.user.isVerified && isEnabled.value);

  const login = async (to: string = "/dashboard"): Promise<boolean> => {
    return new Promise(async (resolve, reject) => {
      if(getEnv("VITE_STARTER_KIT_DEMO")==='true'){
        const { data } = await User.profile();
        store.commit("SET_USER", data);
        if (to) {
          await router.push( "/dashboard" );
          resolve(true);
          return;
        }
      }
      if (!R.isNil(keycloak)) {
        keycloak
          .init({
            onLoad: "login-required",
            promiseType: "native",
            checkLoginIframe: false,
          })
          .then(async (authenticated: any) => {
            store.commit("SET_KEYCLOAK_TOKEN", keycloak.idToken);

            if (authenticated) {
              await Auth.keycloakLogin(keycloak.subject, keycloak.idToken, keycloak.refreshToken)
                .then(async a => {
                  location.hash = "";
                  history.replaceState("", "", location.pathname);
                  store.commit("CLEAR_KEYCLOAK_TOKEN");
                  const { data } = await User.profile();
                  if (!data.isVerified) {
                    await Auth.logout();
                    reject("Your account is not verified");
                  }
                  store.commit("SET_USER", data);
                  // Checks if user has choose demonstrator and accept the appropriate terms

                  if (shouldGoToTerms.value) {
                    await router.push("/terms");
                    resolve(true);
                    return;
                  }

                  if (to) {
                    await router.push(to === "index" ? "/dashboard" : to);
                    resolve(true);
                    return;
                  }
                })
                .catch(async (err: any) => {
                  const message = err.response?.data?.message || err.message;
                  toaster.show(message, "error");
                  await router.push("/");
                  reject(new Error(message));
                });
            }
            resolve(true);
          })
          .catch(() => {
            reject(new Error("Authenticated Failed"));
          });
      }
    });
  };

  const logout = async () => {
    if (!R.isNil(keycloak)) {
      await store.commit("CLEAR_KEYCLOAK_TOKEN");
      await keycloak.logout({ redirectUri: window.location.origin });
    }
  };

  const register = async () => {
    if (!R.isNil(keycloak)) {
      keycloak.init({ onLoad: "login-required", promiseType: "native" });
      await keycloak.register();
    }
  };

  return { isEnabled, isAuthenticated, register, login, logout };
}
