import { Transaction as TransactionAPI } from "@/api";
import { Transaction } from "@/interfaces";
import * as R from "ramda";
import { computed, Ref, ref } from "vue";

export function useTransactions(
  length: number | null = null,
  filter?: Ref<string>
) {
  const transactions = ref<Transaction[]>([]);
  const totalPoints = ref<number>(0);
  const loading = ref<boolean>(false);

  const fetch = () => {
    loading.value = true;

    TransactionAPI.getAll()
      .then((res: any) => {
        transactions.value = res.data;

        transactions.value.forEach((transaction: any) => {
          transaction.shared = false;
          if (transaction.sharedAsset) {
            transaction.type = transaction.sharedAsset?.configuration.shareType;
            transaction.asset = transaction.sharedAsset?.name;
            transaction.category = "Asset";
            transaction.shared = true;
          }
          if (transaction.sharedQuestionnaire) {
            transaction.asset =
              transaction.sharedQuestionnaire?.configuration.title;
            transaction.category = "Questionnaire";
            transaction.shared = true;
          }
        });

        transactions.value = R.sort(
          R.descend(R.prop("timestamp")),
          transactions.value
        );
      })
      .finally(() => (loading.value = false));

      TransactionAPI.getPoints()
      .then((res: any) => {
        totalPoints.value = res.data.points;
      })
  };

  fetch();

  const filteredTransactions = computed(() => {
    let filteredTranactions = R.clone(transactions.value);
    if (filteredTranactions.length === 0) {
      return [];
    }
    if (filter && !R.isNil(filter.value) && !R.isEmpty(filter.value)) {
      if (filter.value === "shared") {
        return filteredTranactions.filter((transaction: any) => {
          return transaction.shared;
        });
      } else if (filter.value === "sold") {
        return filteredTranactions.filter(
          (transaction: any) =>
            !R.isNil(transaction.points) && !R.isEmpty(transaction.points)
        );
      }
    }
    return filteredTranactions;
  });

  const visibleTransactions = computed(() => {
    if (length !== null) {
      return filteredTransactions.value.slice(0, length);
    }
    return filteredTransactions.value;
  });

  return {
    totalPoints,
    transactions,
    loading,
    filteredTransactions,
    visibleTransactions,
    fetch,
  };
}
