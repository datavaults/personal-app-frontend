import { ref } from "vue";
import { SharedAsset } from "@/interfaces";
import { SharedAsset as SharedAssetAPI } from "@/api";

export function useSharedAsset(assetUid: string) {
  const asset = ref<SharedAsset | null>(null);

  SharedAssetAPI.get(assetUid).then((res: any) => {
    asset.value = res.data;
  });
  return { asset };
}
