import { computed, Ref } from "vue";
import * as R from "ramda";
import { countries as countriesList } from "../constants/countries.profile.constant";
import statesList from "../constants/states.json";
import citiesList from "../constants/cities.json";

interface Country {
  id: string;
  name: string;
  sortname: string;
}

interface State {
  id: string;
  name: string;
  country_id: string;
}

interface City {
  id: string;
  name: string;
  state_id: string;
}

export function useWorld(
  country: Ref<string | undefined> | undefined = undefined,
  state: Ref<string | undefined> | undefined = undefined
) {
  //if reverting back to countries JSON, remove .value from countriesList
  const allCountries: Country[] = countriesList as any as Country[];
  const allStates: State[] = statesList as any as State[];
  const allCities: City[] = citiesList as any as City[];
  const statesMap: Record<string, State[]> = {};
  const citiesMap: Record<string, City[]> = {};

  allCountries.sort();
  allStates.sort();
  allCities.sort();

  for (let c = 0; c < allCountries.length; c++) {
    const country: Country = allCountries[c];
    statesMap[country.id] = [];
  }

  for (let s = 0; s < allStates.length; s++) {
    const state: State = allStates[s];
    statesMap[state.country_id].push(state);
    citiesMap[state.id] = [];
  }

  for (let ct = 0; ct < allCities.length; ct++) {
    const city: City = allCities[ct];
    citiesMap[city.state_id].push(city);
  }

  const countries: Ref<Country[]> = computed(() => allCountries);

  const states: Ref<State[]> = computed(() => (country && country.value ? statesMap[country.value] : allStates));

  const cities: Ref<City[]> = computed(() => (state && state.value ? citiesMap[state.value] : allCities));

  const findCountry = (id: string) => allCountries.find((country: Country) => country.id === id);

  const findCountryByName = (name: string) => allCountries.find((country: Country) => country.name === name);

  const findState = (id: string) => allStates.find((state: State) => state.id === id);

  const findStateByName = (name: string) => allStates.find((state: State) => state.name === name);

  const findCity = (id: string) => allCities.find((city: City) => city.id === id);

  const findCityByName = (name: string) => allCities.find((city: City) => city.name === name);

  return {
    countries,
    states,
    cities,
    findCountry,
    findCountryByName,
    findState,
    findStateByName,
    findCity,
    findCityByName,
  };
}
