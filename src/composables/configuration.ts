import { useLocalStorage } from "@/composables/index";
import { ConfigurationLevel } from "@/constants/configuration-level.constant";
import store from "@/store";
import * as R from "ramda";
import { v4 as uuidv4 } from "uuid";
import { ref } from "vue";
import { useRoute } from "vue-router";

export function useConfiguration() {
  const localStorageKey = "datavaults-sharing-configuration";
  const ls = useLocalStorage();
  const route = useRoute();
  const customConfig = {
    level: ConfigurationLevel.CUSTOM,
    anonymisation: {
      anonymise: false,
      pseudoId: null,
      useTPM: false,
      rules: {
        colNames: [],
        buildIDs: [],
        anonLevels: [],
      },
    },
    specificPrice: true,
    price: null,
    licenseType: null,
    standardLicense: null,
    ownLicense: null,
    encryption: false,
    persona: false,
    useTPM: false,
    tpmPseudonym: null,
    shareType: "normal",
    accessPolicies: {
      policyId: "new",
      existingPolicyId: null,
      sector: [],
      type: [],
      size: [],
      continent: [],
      country: [],
      reputation: 0,
    },
  };

  const maxPrivacyConfig = {
    level: ConfigurationLevel.MAX,
    anonymisation: {
      anonymise: true,
      pseudoId: null,
      useTPM: false,
      rules: {
        colNames: [],
        buildIDs: [],
        anonLevels: [],
      },
    },
    specificPrice: true,
    price: null,
    licenseType: null,
    standardLicense: null,
    ownLicense: null,
    encryption: false,
    persona: false,
    useTPM: false,
    shareType: "normal",
    accessPolicies: {
      policyId: "new",
      existingPolicyId: null,
      sector: [],
      type: [],
      size: [],
      continent: [],
      country: [],
      reputation: 0,
    },
  };

  const lowPrivacyConfig = {
    level: ConfigurationLevel.LOW,
    anonymisation: {
      anonymise: false,
      pseudoId: null,
      useTPM: false,
      rules: {
        colNames: [],
        buildIDs: [],
        anonLevels: [],
      },
    },
    specificPrice: true,
    price: null,
    licenseType: null,
    standardLicense: null,
    ownLicense: null,
    encryption: false,
    persona: true,
    useTPM: false,
    shareType: "normal",
    accessPolicies: {
      policyId: "new",
      existingPolicyId: null,
      sector: [],
      type: [],
      size: [],
      continent: [],
      country: [],
      reputation: 0,
    },
  };

  const configuration = ref<any>({
    asset: null,
    assetUUID: uuidv4(),
    assetUid: null,
    name: null,
    description: null,
    keywords: [],
    details: {
      fromExistingConfig: false,
      existingConfig: null,
      existingName: null,
      existingId: null,
    },
    configuration: R.clone(customConfig),
    fieldMapping: [],
  });

  const chooseConfiguration = (configChoice: any) => {
    configuration.value.configuration = R.clone(configChoice);
  };

  const assignConfiguration = (configName: string) => {
    if (configName === ConfigurationLevel.MAX) chooseConfiguration(maxPrivacyConfig);
    if (configName === ConfigurationLevel.LOW) chooseConfiguration(lowPrivacyConfig);
    if (configName === ConfigurationLevel.CUSTOM) chooseConfiguration(customConfig);
  };

  const currentStep = R.has("step", route.query) ? parseInt(String(route.query.step), 10) - 1 : 0;
  const id = ref<string | null>(R.has("id", route.query) ? String(route.query.id) : null);

  //try to load from local storage
  if (!R.isNil(id.value) && R.isNil(store.getters.getConfigurationUuid)) {
    const localStorageConfiguration = ls.getObject(localStorageKey);
    if (!R.isNil(localStorageConfiguration)) {
      store.commit("ADD_CONFIGURATION", localStorageConfiguration);
    }
  }
  if (!R.isNil(id.value) && store.getters.getConfigurationUuid === id.value) {
    configuration.value = store.getters.getConfiguration;
  } else {
    id.value = uuidv4();
    store.commit("ADD_CONFIGURATION", {
      uuid: id.value,
      upToStep: 1,
      configuration: configuration.value,
    });
    ls.setObject(localStorageKey, {
      uuid: id.value,
      upToStep: 1,
      configuration: configuration.value,
    });
  }

  const checkStep = () => {
    // TODO remove if no longer needed
    // if (!R.isNil(ls.getObject(localStorageKey))) {
    //   const currentUpToStep = ls.getObject(localStorageKey).upToStep;
    //   if (currentUpToStep && currentUpToStep < currentStep + 1) {
    //     return {
    //       isStepValid: false,
    //       validStep: Step.findByPosition(currentUpToStep - 1).getPageName(),
    //     };
    //   }
    // }
    return { isStepValid: true, validStep: null };
  };

  const save = () => {
    let newUpToStep = store.getters.getUpToStep;
    if (currentStep + 1 >= newUpToStep) newUpToStep += 1;
    store.commit("UPDATE_CONFIGURATION", {
      uuid: id.value,
      upToStep: newUpToStep,
      configuration: configuration.value,
    });
    ls.setObject(localStorageKey, {
      uuid: id.value,
      upToStep: newUpToStep,
      configuration: configuration.value,
    });
  };

  const getNewConfiguration = () => {
    return customConfig;
  };

  return {
    id,
    configuration,
    currentStep,
    save,
    getNewConfiguration,
    checkStep,
    chooseConfiguration,
    customConfig,
    maxPrivacyConfig,
    lowPrivacyConfig,
    assignConfiguration,
  };
}
