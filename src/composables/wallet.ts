import { ref, computed } from "vue";
import { Wallet as WalletAPI } from "@/api";
import store from "@/store";

export function useWallet() {
  //(custAccountId: string) {
  const loading = ref<boolean>(false);
  const totalPoints = ref<number>();
  const user = computed(() => store.state.user);
  const custAccountId = computed(() => {
    return user.value.sub;
  });

  const refresh = () => {
    loading.value = true;

    //...
    loading.value = false;
  };

  /*const fetch =() {

    };*/

  let ecoinTypeList: Array<string>;
  ecoinTypeList = ["points"];
  const getAccount = () => {
    WalletAPI.createAccount(custAccountId.value, {
      ecoinTypes: ecoinTypeList,
      name: custAccountId.value,
    }).then(response => {
      totalPoints.value = response.data.wallet.numEcoins;
    });
  };

  getAccount();
  refresh();

  return { loading, totalPoints };
}
