import { computed, Ref, ref, watchEffect } from "vue";
import { Anonymiser as AnonymiserAPI } from "@/api";
import * as R from "ramda";

export function useAnonymisationPreview(
  fieldsToAnonymize: Ref<any>,
  sampleData: Ref<any>,
  buildIDs: Ref<any>,
  anonLevels: Ref<any>,
  metadata: Ref<any>
) {
  const anonymisationPreview = ref<any | null>(null);

  const configuration = computed(() => {
    const columnNames = Object.keys(fieldsToAnonymize.value).filter(
      (field: string) =>
        fieldsToAnonymize.value[field] &&
        !R.isNil(buildIDs.value[field]) &&
        !R.isNil(anonLevels.value[field])
    );
    const hierarchies = [];
    const levels = [];

    for (let c = 0; c < columnNames.length; c++) {
      const column = columnNames[c];
      if (R.has(column, buildIDs.value))
        hierarchies.push(buildIDs.value[column]);
      if (R.has(column, anonLevels.value))
        levels.push(Number(anonLevels.value[column]));
    }

    return {
      dataset: sampleData.value,
      structure: metadata.value,
      colNames: columnNames,
      buildIDs: hierarchies,
      anonLevels: levels,
    };
  });

  //TODO: Catch errors to display message
  const fetch = () => {
    return new Promise<void>((resolve, reject) => {
      AnonymiserAPI.getPreview(configuration.value)
        .then((res: any) => {
          if (res.response?.status && res.response.status >= 400) {
            reject(res);
            return;
          }
          anonymisationPreview.value = res.data;
          resolve();
        })
        .catch((e) => {
          reject(e);
        });
    });
  };

  const clear = () => {
    anonymisationPreview.value = null;
  };

  const canAnonymise = ref<boolean>(false);
  watchEffect(() => {
    canAnonymise.value =
      configuration.value.colNames.length > 0 &&
      configuration.value.buildIDs.length ===
        configuration.value.colNames.length &&
      configuration.value.anonLevels.length ===
        configuration.value.colNames.length;
  });

  return { anonymisationPreview, configuration, canAnonymise, fetch, clear };
}
