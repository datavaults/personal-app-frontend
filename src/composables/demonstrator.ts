import dayjs from "dayjs";
import isSameOrAfter from "dayjs/plugin/isSameOrAfter";
import { Andaman7Terms, MiwenergiaTerms, PratoTerms, PiraeusTerms, OlympiacosTerms, ExternalParticipantTerms } from "@/components/terms";
import { computed, Ref } from "vue";
import * as R from "ramda";
import store from "@/store";

export enum DemonstratorsEnum {
  PRATO = "Prato",
  OLYMPIACOS = "Olympiacos",
  PIRAEUS = "Piraeus",
  MIWENERGIA = "Miwenergia",
  ANDAMAN7 = "Andaman7",
  EXTERNALPARTICIPANT = "External Participant",
}

export function useDemonstrators(selecteDemonstator?: Ref<string>) {
  dayjs.extend(isSameOrAfter);
  const demonstrators: Record<DemonstratorsEnum, { component: any; lastUpdated: Date }> = {} as any;

  demonstrators[DemonstratorsEnum.PRATO] = {
    component: PratoTerms,
    lastUpdated: dayjs("2022-07-04T00:00:00.000Z").toDate(),
  };

  demonstrators[DemonstratorsEnum.OLYMPIACOS] = {
    component: OlympiacosTerms,
    lastUpdated: dayjs("2022-07-04T00:00:00.000Z").toDate(),
  };

  demonstrators[DemonstratorsEnum.PIRAEUS] = {
    component: PiraeusTerms,
    lastUpdated: dayjs("2022-07-04T00:00:00.000Z").toDate(),
  };

  demonstrators[DemonstratorsEnum.MIWENERGIA] = {
    component: MiwenergiaTerms,
    lastUpdated: dayjs("2022-07-04T00:00:00.000Z").toDate(),
  };

  demonstrators[DemonstratorsEnum.ANDAMAN7] = {
    component: Andaman7Terms,
    lastUpdated: dayjs("2022-07-04T00:00:00.000Z").toDate(),
  };

  demonstrators[DemonstratorsEnum.EXTERNALPARTICIPANT] = {
    component: ExternalParticipantTerms,
    lastUpdated: dayjs("2022-07-04T00:00:00.000Z").toDate(),
  };

  const demonstratorName = computed(() => selecteDemonstator?.value || store.getters.retrieveDemonstrator);
  const lastAcceptance = computed(() => store.getters.lastTermsAcceptance);

  const demonstrator = computed(() => {
    if (!demonstratorName.value) return null;
    const foundDemonstratorKey: DemonstratorsEnum | undefined = (
      Object.keys(demonstrators) as DemonstratorsEnum[]
    ).find(
      (demonstratorKey: DemonstratorsEnum) => 
        demonstratorKey.toLowerCase() === demonstratorName.value.toLowerCase()
    );
    return foundDemonstratorKey ? demonstrators[foundDemonstratorKey] : null;
  });

  const isUpToDate = (lastAcceptance: Date): boolean | undefined => {
    return !!demonstrator.value && dayjs(lastAcceptance).isSameOrAfter(dayjs(demonstrator.value.lastUpdated));
  };

  const sourceComponent = computed(() => {
    return demonstrator.value?.component;
  });

  const shouldGoToTerms = computed(() => !demonstratorName || !isUpToDate(lastAcceptance.value));

  return {
    demonstratorName,
    demonstrator,
    demonstrators,
    sourceComponent,
    shouldGoToTerms,
    isUpToDate,
  };
}
