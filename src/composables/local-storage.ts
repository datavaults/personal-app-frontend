import * as R from "ramda";

export function useLocalStorage() {
  const get = (key: string) => {
    const itemFromStore = localStorage.getItem(key);
    if (R.isNil(itemFromStore)) {
      return null;
    }
    return itemFromStore;
  };

  const set = (key: string, value: any) => {
    localStorage.setItem(key, value);
  };

  const getObject = (key: string) => {
    const itemFromStore = localStorage.getItem(key);
    if (R.isNil(itemFromStore)) {
      return null;
    }
    return JSON.parse(itemFromStore);
  };

  const setObject = (key: string, value: any) => {
    localStorage.setItem(key, JSON.stringify(value));
  };

  const clear = (key: string) => {
    localStorage.removeItem(key);
  };

  return { get, set, getObject, setObject, clear };
}
