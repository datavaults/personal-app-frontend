import { ref } from "vue";
import { DataFetcher } from "@/api";
import { SourceOption } from "@/interfaces";

export function useDataFetcherSources() {
  const sourceOptions = ref<SourceOption[]>([]);
  const loading = ref<boolean>(false);

  const refresh = () => {
    loading.value = true;
    DataFetcher.getSourceOptions().then(
      ({ data }: { data: SourceOption[] }) => {
        sourceOptions.value = data;
        loading.value = false;
      }
    );
  };

  refresh();

  return { sourceOptions, loading, refresh };
}
