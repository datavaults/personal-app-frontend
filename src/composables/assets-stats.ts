interface MonthlyAsset {
  month: string;
  totalshared: number;
  totalsold: number;
  totalPoint: number;
}

import { AssetsStats } from "@/api";
import { ref } from "vue";

export function useAssetsStats() {
  const years = ref<string[]>([]);
  const lastYearInList = ref<string>();
  const totalAssets = ref<number>();
  const totalSharedAssets = ref<number>();
  const totalSoldAssets = ref<number>();
  const totalUnsoldAssets = ref<number>();
  const monthlySharedAssets = ref<number[]>([]);
  const monthlySoldAssets = ref<number[]>([]);
  const loading = ref<boolean>(false);
  const totalSharedAssetsCard = ref<number>();
  const totalSoldCard = ref<number>();
  const totalPointsCard = ref<any>();
  const totalUniqueSharedAssets = ref<number>();
  const monthlyQPoints = ref<number>();

  const getYears = (): void => {
    loading.value = true;
    AssetsStats.getYears()
      .then((res: any) => {
        const yearSet = new Set(res.data);
        years.value = Array.from(yearSet) as any;
        years.value.sort((a, b) => Number(b) - Number(a));
        lastYearInList.value = years.value[0];
      })
      .finally(() => (loading.value = false));
  };

  const refresh = (year: string): void => {
    loading.value = true;
    const promiseTotalAssets = AssetsStats.totalAssets(`${year}-01-01T00:00:00.000Z`, `${year}-12-31T00:00:00.000Z`);
    const promiseTotalSharedAssetsByYear = AssetsStats.totalSharedAssets(
      `${year}-01-01T00:00:00.000Z`,
      `${year}-12-31T00:00:00.000Z`
    );
    const promiseMonthlyAssetStats = AssetsStats.sharedAsset(year);

    Promise.all([promiseTotalAssets, promiseTotalSharedAssetsByYear, promiseMonthlyAssetStats])
      .then((res: any) => {
        totalAssets.value = res[0].data;
        totalSharedAssets.value = res[1].data[0].total;
        totalSoldAssets.value = res[1].data[0].sold;
        totalUnsoldAssets.value = res[1].data[0].unsold;
        totalUniqueSharedAssets.value = res[1].data[0].totalUnique;
        monthlySharedAssets.value = res[2].data.map((item: MonthlyAsset) => item.totalshared);
        monthlySoldAssets.value = res[2].data.map((item: MonthlyAsset) => item.totalsold);
        monthlyQPoints.value = res[2].data.map((item: MonthlyAsset) => item.totalPoint);
      })
      .finally(() => (loading.value = false));
  };

  AssetsStats.cardsTotalSharedAssets()
    .then((res: any) => {
      loading.value = true;
      totalSharedAssetsCard.value = res.data[0].total;
      totalSoldCard.value = res.data[0].sold;
      totalPointsCard.value = res.data[0].totalPoint;
    })
    .finally(() => (loading.value = false));

  return {
    getYears,
    years,
    lastYearInList,
    refresh,
    totalAssets,
    totalSharedAssets,
    totalUniqueSharedAssets,
    totalSoldAssets,
    totalUnsoldAssets,
    monthlySharedAssets,
    monthlySoldAssets,
    totalSharedAssetsCard,
    totalSoldCard,
    totalPointsCard,
    monthlyQPoints,
    loading
  };
}
