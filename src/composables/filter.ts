import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
import isSameOrAfter from "dayjs/plugin/isSameOrAfter";
import isSameOrBefore from "dayjs/plugin/isSameOrBefore";
import FuzzySearch from "fuzzy-search";
import * as R from "ramda";
import { computed, ref, Ref } from "vue";

export function useFilter(
  data: Ref<any[]> | any[],
  options?: {
    searchTerms?: string;
    currentPage?: number;
    pageSize?: number;
    searchFields?: string[];
    sortFields?: { key: string; type: string }[];
    sortBy?: string | null;
    sortDirection?: string;
    ascendingSortDirectionKey?: string;
    descendingSortDirectionKey?: string;
    filterFields?: { key: string; type: string; method: string }[];
  }
) {
  dayjs.extend(isBetween);
  dayjs.extend(isSameOrAfter);
  dayjs.extend(isSameOrBefore);
  const DEFAULT_ASCENDING_SORT_DIRECTION_KEY = "asc";
  const DEFAULT_DESCENDING_SORT_DIRECTION_KEY = "desc";

  const searchTerms = ref<string>(options?.searchTerms || "");
  const currentPage = ref<number>(options?.currentPage || 1);
  const pageSize = ref<number>(options?.pageSize || 10);
  const sortBy = ref<string | null>(options?.sortBy || null);
  const ascendingSortDirectionKey =
    options?.ascendingSortDirectionKey || DEFAULT_ASCENDING_SORT_DIRECTION_KEY;
  const descendingSortDirectionKey =
    options?.ascendingSortDirectionKey || DEFAULT_DESCENDING_SORT_DIRECTION_KEY;
  const sortDirection = ref<string>(
    options?.sortDirection || ascendingSortDirectionKey
  );

  const filters = ref<any>({});

  const sortFields = options?.sortFields
    ? options.sortFields.reduce(
        (acc: any, sortField: { key: string; type: string }) => {
          acc[sortField.key] = sortField.type;
          return acc;
        },
        {}
      )
    : {};

  const rawData = computed(() =>
    R.is(Array, data) ? data : (data as Ref).value
  );

  const parse = (val: any, type: string) => {
    if (type === "datetime") {
      return dayjs(val).toDate();
    } else if (type === "integer") {
      return parseInt(val, 10);
    } else if (type === "double") {
      return parseFloat(val);
    }
    return val;
  };

  const filterFields = options?.filterFields
    ? options?.filterFields?.reduce(
        (acc: any, item: { key: string; type: string; method: string }) => {
          acc[item.key] = item;
          return acc;
        },
        {}
      )
    : {};

  const filteredData = computed(() => {
    let filtered = rawData.value;

    if (
      options &&
      !R.isNil(options.searchFields) &&
      !R.isEmpty(options.searchFields) &&
      !R.isNil(searchTerms.value) &&
      !R.isEmpty(searchTerms.value)
    ) {
      const fuzzySearch = new FuzzySearch(filtered, options.searchFields, {
        caseSensitive: false,
      });
      filtered = fuzzySearch.search(searchTerms.value);
    }
    Object.keys(filterFields).forEach((field: string) => {
      const filter = filterFields[field];
      const filterValue = filters.value[field];
      if (filterValue) {
        if (filter.type === "string" && filter.method === "exact") {
          filtered = filtered.filter((data: any) => {
            const removeWhiteSpace = filterValue.replace(/\s/g, '');
            return data.sourceType.toUpperCase() === removeWhiteSpace.toUpperCase();
          });
        } else if (filter.type === "datetime") {
          if (filter.method === "from")
            filtered = filtered.filter((data: any) => {
              return dayjs(dayjs(data.updatedAt)).isSameOrAfter(filterValue, 'day');
            });
          if (filter.method === "to")
            filtered = filtered.filter((data: any) => {
              return dayjs(dayjs(data.updatedAt)).isSameOrBefore(filterValue, 'day');
            });
        }
      }
    });

    if (!R.isNil(sortBy.value) && R.has(sortBy.value, sortFields)) {
      const sortFieldType: any = sortFields[sortBy.value];
      const isAscending: boolean =
        sortDirection.value === ascendingSortDirectionKey;
      return R.sort((a: any, b: any) => {
        let aVal = null;
        let bVal = null;
        if (!R.isNil(a) && sortBy.value) {
          aVal = parse(a[sortBy.value], sortFieldType);
        }
        if (!R.isNil(b) && sortBy.value) {
          bVal = parse(b[sortBy.value], sortFieldType);
        }
        if (!R.isNil(aVal) && R.isNil(bVal)) {
          return isAscending ? 1 : -1;
        }
        if (R.isNil(aVal) && !R.isNil(bVal)) {
          return isAscending ? -1 : 1;
        }
        if (R.isNil(aVal) && R.isNil(bVal)) {
          return 0;
        }

        if (["datetime", "integer", "double"].includes(sortFieldType)) {
          return isAscending ? aVal - bVal : bVal - aVal;
        }
        return isAscending
          ? aVal.localeCompare(bVal)
          : bVal.localeCompare(aVal);
      }, filtered);
    }

    if(filtered.length > 0) {
      return filtered.sort((a: any, b: any) => {
        return (new Date(b.updatedAt) as any) - (new Date(a.updatedAt) as any);
      });
    } else {
      return [];
    }
  });

  const visibleData = computed(() => {
    const startIndex = (currentPage.value - 1) * pageSize.value;
    const endIndex = startIndex + pageSize.value;
    return filteredData.value.slice(startIndex, endIndex);
  });

  const totalPages = computed(() => {
    return Math.ceil(rawData.value.length / pageSize.value);
  });

  const next = () => {
    if (currentPage.value < totalPages.value) {
      currentPage.value += 1;
    }
  };

  const previous = () => {
    if (currentPage.value > 1) {
      currentPage.value -= 1;
    }
  };

  const goTo = (page: number) => {
    if (page <= totalPages.value) {
      currentPage.value = page;
    }
  };

  return {
    filteredData,
    visibleData,
    searchTerms,
    sortBy,
    sortDirection,
    currentPage,
    pageSize,
    totalPages,
    filters,
    next,
    previous,
    goTo,
  };
}
