import * as R from "ramda";

export function useUtilities() {
  const splitAndCapitalize = (s: string, separator: string = "-") => {
    let result = R.replace(new RegExp(separator, "g"), " ", s);
    return result.charAt(0).toUpperCase() + result.slice(1);
  };

  const colorHexFromPercentage = (percentage: number) => {
    return `hsl(${percentage}, 100%, 40%)`;
  };

  const imageToBase64 = (buffer: any) => {
    const typedArray = new Uint8Array(buffer);
    const stringCharacters = typedArray.reduce((data: string, byte: any) => {
      return data + String.fromCharCode(byte);
    }, "");

    return btoa(stringCharacters);
  };

  return { splitAndCapitalize, colorHexFromPercentage, imageToBase64 };
}
