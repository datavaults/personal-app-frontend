import { ref } from "vue";
import { Anonymiser as AnonymiserAPI } from "@/api";
import { useUtilities } from "@/composables/utilities";

export function useAnonymisation() {
  const hierarchiesByType = ref<any>({});
  const { splitAndCapitalize } = useUtilities();

  AnonymiserAPI.getHierarchiesByType().then((res: any) => {
    for (const ht in Object.keys(res.data)) {
      const hierarchyType = Object.keys(res.data)[ht];
      hierarchiesByType.value[hierarchyType] = res.data[hierarchyType].map(
        (option: string) => {
          return { key: option, label: splitAndCapitalize(option, "-") };
        }
      );
    }
  });

  return { hierarchiesByType };
}
