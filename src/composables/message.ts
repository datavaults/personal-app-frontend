import { computed, Ref, ref, watch } from "vue";
import * as R from "ramda";
import { Message } from "@/interfaces";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { Messages as MessagesAPI } from "@/api";
import { useMessages } from "@/composables/messages";
import { MessageTypeWrapper } from "@/constants";

dayjs.extend(relativeTime);

export function useMessage(messageId: Ref<number>) {
  const message = ref<Message | null>(null);
  const messages = useMessages();

  const messageDate = computed(() =>
    message.value?.timestamp
      ? dayjs(message.value.timestamp as Date).format("DD MMM YYYY @ HH:mm")
      : null
  );
  const messageDateFromNow = computed(() =>
    message.value?.timestamp
      ? dayjs(message.value.timestamp as Date).fromNow()
      : null
  );

  const messageType = computed(() =>
    R.isNil(message.value)
      ? null
      : MessageTypeWrapper.find(message.value.messageType)
  );

  const deleteMessage = async () => {
    if (message.value) {
      await MessagesAPI.delete(message.value.id as number);
    }
  };

  const markAsRead = async () => {
    if (message.value && R.isNil(message.value.readAt)) {
      await MessagesAPI.markAsRead(message.value.id as number);
      messages.refresh();
    }
    return false;
  };

  const refresh = () => {
    MessagesAPI.get(messageId.value).then((res: any) => {
      message.value = res.data;
    });
  };

  watch(
    () => messageId.value,
    () => {
      messages.refresh();
      refresh();
    }
  );

  refresh();

  return {
    message,
    messageType,
    messageDate,
    messageDateFromNow,
    deleteMessage,
    markAsRead,
    refresh,
  };
}
