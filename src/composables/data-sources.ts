import { DataFetcher } from "@/api";
import { CollectionFrequencyWrapper } from "@/constants";
import { computed, ref, Ref } from "vue";
import * as R from "ramda";
import {
  Andaman7Connection,
  FacebookConnection,
  MiwenergiaConnection,
  TwitterConnection,
  PratoConnection,
  OlympiakosConnection,
} from "../components/data-fetcher";

export enum DataSourcesEnum {
  ANDAMAN7 = "ANDAMAN7",
  MIWENERGIA = "MIWENERGIA",
  TWITTER = "TWITTER",
  FACEBOOK = "FACEBOOK",
  MOBILEAPP = "MOBILEAPP",
  PRATO = "PRATO",
  UPLOAD = "UPLOAD",
  OLYMPIAKOS = "OLYMPIAKOS",
}
export interface DataSourceOption {
  id: string;
  name: string;
  url: string;
  auth_type: string;
  auth_properties: string[];
  sourceType: DataSourcesEnum;
  component: any;
  collectionFrequencyOptions: CollectionFrequencyWrapper[];
  textColorClass: string;
  backgroundColorClass: string;
}

export interface SourceOption {
  id: string;
  name: string;
  sourcetype: string;
  url: string;
  auth_type: string;
  auth_properties: string[];
}
const firstTime: Ref<boolean> = ref<boolean>(true);
const onlineSources: Ref<any> = ref<DataSourceOption[]>([]);

export function useDataSource(selected?: Ref<any>) {
  const loading: Ref<boolean> = ref<boolean>(false);

  const sourceOptions: Record<
    DataSourcesEnum,
    {
      component: any;
      collectionFrequencyOptions: CollectionFrequencyWrapper[];
      textColorClass: string;
      backgroundColorClass: string;
    }
  > = {} as any as Record<
    DataSourcesEnum,
    {
      component: any;
      collectionFrequencyOptions: CollectionFrequencyWrapper[];
      textColorClass: string;
      backgroundColorClass: string;
    }
  >;

  sourceOptions[DataSourcesEnum.ANDAMAN7] = {
    component: Andaman7Connection,
    collectionFrequencyOptions: [CollectionFrequencyWrapper.DAY, CollectionFrequencyWrapper.HOUR],
    textColorClass: "text-indigo-800",
    backgroundColorClass: "bg-indigo-100",
  };

  sourceOptions[DataSourcesEnum.MIWENERGIA] = {
    component: MiwenergiaConnection,
    collectionFrequencyOptions: [
      CollectionFrequencyWrapper.ONCE,
      CollectionFrequencyWrapper.HOUR,
      CollectionFrequencyWrapper.DAY,
      CollectionFrequencyWrapper.WEEK,
      CollectionFrequencyWrapper.MONTH,
    ],
    textColorClass: "text-blue-800",
    backgroundColorClass: "bg-blue-100",
  };

  sourceOptions[DataSourcesEnum.TWITTER] = {
    component: TwitterConnection,
    collectionFrequencyOptions: CollectionFrequencyWrapper.all(),
    textColorClass: "text-sky-800",
    backgroundColorClass: "bg-sky-100",
  };

  sourceOptions[DataSourcesEnum.FACEBOOK] = {
    component: FacebookConnection,
    collectionFrequencyOptions: CollectionFrequencyWrapper.all(),
    textColorClass: "text-blue-800",
    backgroundColorClass: "bg-blue-100",
  };

  sourceOptions[DataSourcesEnum.MOBILEAPP] = {
    component: null,
    collectionFrequencyOptions: CollectionFrequencyWrapper.all(),
    textColorClass: "text-purple-800",
    backgroundColorClass: "bg-purple-100",
  };

  sourceOptions[DataSourcesEnum.PRATO] = {
    component: PratoConnection,
    collectionFrequencyOptions: [CollectionFrequencyWrapper.ONCE],
    textColorClass: "text-red-800",
    backgroundColorClass: "bg-red-100",
  };

  sourceOptions[DataSourcesEnum.OLYMPIAKOS] = {
    component: OlympiakosConnection,
    collectionFrequencyOptions: CollectionFrequencyWrapper.all(),
    textColorClass: "text-sky-800",
    backgroundColorClass: "bg-sky-100",
  };

  sourceOptions[DataSourcesEnum.UPLOAD] = {
    component: null,
    collectionFrequencyOptions: [],
    textColorClass: "text-green-800",
    backgroundColorClass: "bg-green-100",
  };

  const sources = computed(() => {
    return [
      ...onlineSources.value,
      {
        ...sourceOptions[DataSourcesEnum.UPLOAD],
        id: DataSourcesEnum.UPLOAD,
        name: "File Upload",
        sourceType: DataSourcesEnum.UPLOAD,
      },
    ];
  });

  const refresh = () => {
    loading.value = true;
    DataFetcher.getSourceOptions().then(({ data }: { data: SourceOption[] }) => {
      onlineSources.value = data.reduce((acc: DataSourceOption[], s: SourceOption) => {
        if (R.has(s.sourcetype.toUpperCase(), sourceOptions)) {
          const sourceType: DataSourcesEnum = s.sourcetype.toUpperCase() as DataSourcesEnum;
          const sourceConfiguration: {
            component: any;
            collectionFrequencyOptions: CollectionFrequencyWrapper[];
            textColorClass: string;
            backgroundColorClass: string;
          } = sourceOptions[sourceType];

          acc.push({
            ...sourceConfiguration,
            ...s,
            sourceType,
          });
        }

        return acc;
      }, []);
      loading.value = false;
    });
  };

  const sourceFromSourceType = (sourceType: string) => {
    return sources.value.find((s: DataSourceOption) => {
      return s.sourceType.toUpperCase() === sourceType.toUpperCase();
    });
  };

  const source: Ref<DataSourceOption | undefined> = computed(() => {
    if (selected?.value?.sourceType)
      return sources.value.find((s: DataSourceOption) => {
        return s.sourceType.toUpperCase() === selected.value.sourceType.toUpperCase();
      });
    return undefined;
  });

  const component = computed(() => source.value?.component);

  const backgroundColorClass = computed(() => source.value?.backgroundColorClass);
  const textColorClass = computed(() => source.value?.textColorClass);

  const collectionFrequencyOptions = computed(() => source.value?.collectionFrequencyOptions);

  if (firstTime.value) {
    refresh();
    firstTime.value = false;
  }

  return {
    onlineSources,
    source,
    component,
    collectionFrequencyOptions,
    backgroundColorClass,
    textColorClass,
    sourceFromSourceType,
  };
}
