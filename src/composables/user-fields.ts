import { computed, Ref } from "vue";
//DO NOT DELETE FOLLOWING COMMENTS for now, in case reverting is necessary
// import occupationsList from "../constants/occupations.json";
import { occupations as occupationsList } from "../constants/occupations.profile.constant";
import civilStatusList from "../constants/civil-status.json";
// import culturalInterestList from "../constants/cultural-interest.json";
import { culture as culturalInterestList } from "../constants/cultural-interest.profile.constant";
// import disabilitesList from "../constants/disabilities.json";
import { disabilities as disabilitesList } from "../constants/disabilities.profile.constant";
// import educationList from "../constants/education.json";
import { education as educationList } from "../constants/education.profile.constant";
// import transportationList from "../constants/transportation-means.json";
import { transportation as transportationList } from "../constants/transportation.profile.constant";

interface Occupation {
  id: number;
  value: string;
  label: string;
}

interface CivilStatus {
  id: number;
  value: string;
  label: string;
}

interface CulturalInterest {
  id: number;
  value: string;
  label: string;
}

interface Disabilities {
  id: number;
  value: string;
  label: string;
}

interface Education {
  id: number;
  value: string;
  label: string;
}

interface Transportation {
  id: number;
  value: string;
  label: string;
}

export function useUserFields() {
  //if reverting back to json, remove .value where necessary
  const allOccupations: Occupation[] = occupationsList as any as Occupation;
  const allCivilStatus: CivilStatus[] = civilStatusList as any as CivilStatus;
  const allCulturalInterest: CulturalInterest[] = culturalInterestList as any as CulturalInterest;
  const allDisabilities: Disabilities[] = disabilitesList as any as Disabilities;
  const allEducation: Education[] = educationList as any as Education;
  const allTrasportation: Transportation[] = transportationList as any as Transportation;

  allOccupations.sort();
  allCivilStatus.sort();
  allCulturalInterest.sort();
  allDisabilities.sort();
  allEducation.sort();
  allTrasportation.sort();

  //Start conversion from string to id
  const findOccupation = (value: string[]) => {
    let acc: any = [];
    allOccupations.forEach((occupation: Occupation) => {
      if (occupation.value === value[0]) {
        acc.push(occupation.id);
      }
    });
    return acc;
  };
  const findCivilStatus = (value: string[]) => {
    let acc: any = [];
    allCivilStatus.forEach((civilStatus: CivilStatus) => {
      if (civilStatus.value === value[0]) {
        acc.push(civilStatus.id);
      }
    });
    return acc;
  };
  const findCulturalInterst = (value: string[]) =>
    allCulturalInterest.reduce((acc: any, culturalInterest: CulturalInterest) => {
      Object.values(value).forEach((item: any) => {
        if (culturalInterest.value === item) {
          acc.push(culturalInterest.id);
        }
      });
      return acc;
    }, []);
  const findDisabilities = (value: string[]) =>
    allDisabilities.reduce((acc: any, disability: Disabilities) => {
      Object.values(value).forEach((item: any) => {
        if (disability.value === item) {
          acc.push(disability.id);
        }
      });
      return acc;
    }, []);
  const findEducation = (value: string[]) => {
    let acc: any = [];
    allEducation.forEach((education: Education) => {
      if (education.value === value[0]) {
        acc.push(education.id);
      }
    });
    return acc;
  };
  const findTransportation = (value: string[]) =>
    allTrasportation.reduce((acc: any, transportation: Transportation) => {
      Object.values(value).forEach((item: any) => {
        if (transportation.value === item) {
          acc.push(transportation.id);
        }
      });
      return acc;
    }, []);
  //End id conversion

  //Start conversion from id to string
  const findStringOccupation = (value: number[]) =>
    allOccupations.reduce((acc: any, occupation: Occupation) => {
      value.forEach((item: any) => {
        if (occupation.id === item) {
          acc.push(occupation.value);
        }
      });
      return acc;
    }, []);
  const findStringCivilStatus = (value: number[]) =>
    allCivilStatus.reduce((acc: any, civilStatus: CivilStatus) => {
      value.forEach((item: any) => {
        if (civilStatus.id === item) {
          acc.push(civilStatus.value);
        }
      });
      return acc;
    }, []);
  const findStringCulturalInterst = (value: number[]) =>
    allCulturalInterest.reduce((acc: any, culturalInterest: CulturalInterest) => {
      value.forEach((item: any) => {
        if (culturalInterest.id === item) {
          acc.push(culturalInterest.value);
        }
      });
      return acc;
    }, []);
  const findStringDisabilities = (value: number[]) =>
    allDisabilities.reduce((acc: any, disability: Disabilities) => {
      value.forEach((item: any) => {
        if (disability.id === item) {
          acc.push(disability.value);
        }
      });
      return acc;
    }, []);
  const findStringEducation = (value: number[]) =>
    allEducation.reduce((acc: any, education: Education) => {
      value.forEach((item: any) => {
        if (education.id === item) {
          acc.push(education.value);
        }
      });
      return acc;
    }, []);
  const findStringTransportation = (value: number[]) =>
    allTrasportation.reduce((acc: any, transportation: Transportation) => {
      value.forEach((item: any) => {
        if (transportation.id === item) {
          acc.push(transportation.value);
        }
      });
      return acc;
    }, []);
  //End string conversion
  const occupations: Ref<Occupation[]> = computed(() => allOccupations);
  const civilStatus: Ref<CivilStatus[]> = computed(() => allCivilStatus);
  const culturalInterests: Ref<CulturalInterest[]> = computed(() => allCulturalInterest);
  const disabilities: Ref<Disabilities[]> = computed(() => allDisabilities);
  const education: Ref<Education[]> = computed(() => allEducation);
  const transportation: Ref<Transportation[]> = computed(() => allTrasportation);

  return {
    occupations,
    civilStatus,
    culturalInterests,
    disabilities,
    education,
    transportation,
    findOccupation,
    findCivilStatus,
    findCulturalInterst,
    findDisabilities,
    findEducation,
    findTransportation,
    findStringOccupation,
    findStringCivilStatus,
    findStringCulturalInterst,
    findStringDisabilities,
    findStringEducation,
    findStringTransportation,
  };
}
