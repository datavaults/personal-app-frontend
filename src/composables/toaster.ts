import { ref } from "vue";
import { v4 as uuidv4 } from "uuid";
import {
  InformationCircleIcon,
  ExclamationIcon,
  CheckCircleIcon,
  XCircleIcon,
} from "@heroicons/vue/outline";

const enum ToastTypeEnum {
  Info = "info",
  Warning = "warning",
  Success = "success",
  Error = "error",
}

class ToastType {
  static readonly INFO = new ToastType(
    ToastTypeEnum.Info,
    "blue",
    InformationCircleIcon
  );
  static readonly WARNING = new ToastType(
    ToastTypeEnum.Warning,
    "yellow",
    ExclamationIcon
  );
  static readonly SUCCESS = new ToastType(
    ToastTypeEnum.Success,
    "green",
    CheckCircleIcon
  );
  static readonly ERROR = new ToastType(
    ToastTypeEnum.Error,
    "red",
    XCircleIcon
  );

  private type: ToastTypeEnum;
  private colour: string;
  private icon: any;

  constructor(type: ToastTypeEnum, colour: string, icon: any) {
    this.type = type;
    this.colour = colour;
    this.icon = icon;
  }

  public static all(): ToastType[] {
    return [this.INFO, this.WARNING, this.SUCCESS, this.ERROR];
  }

  public static find(type: string) {
    const foundType = this.all().find((t: ToastType) => t.type === type);
    if (!foundType) {
      throw Error(`Type ${type} not found`);
    }
    return foundType;
  }
}

export class Toast {
  public id: string;
  public message: string;
  public title: string | null;
  public type: ToastType;
  public lifespan: number;

  constructor(
    message: string,
    title: string | null = null,
    type: ToastType = ToastType.INFO,
    lifespan: number = 5000
  ) {
    this.id = uuidv4();
    this.message = message;
    this.title = title;
    this.type = type;
    this.lifespan = lifespan;
  }
}

const toasts = ref<Toast[]>([]);

export function useToaster() {
  const show = (
    message: string,
    type: string = ToastTypeEnum.Info,
    title: string | null = null,
    lifespan: number = 5000
  ) => {
    const t: ToastType = ToastType.find(type);
    const toast = new Toast(message, title, t, lifespan);
    toasts.value.push(toast);
  };

  const closeToast = (toast: Toast) => {
    toasts.value = toasts.value.filter((t: any) => t.id !== toast.id);
  };

  const clear = () => {
    toasts.value = [];
  };

  return { ToastType, toasts, show, clear, closeToast };
}
