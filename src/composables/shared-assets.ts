import { ref } from "vue";
import { SharedAsset } from "@/interfaces";
import { SharedAsset as SharedAssetAPI } from "@/api";

export function useSharedAssets() {
  const assets = ref<SharedAsset[]>([]);
  const loading = ref<boolean>(false);

  const refresh = () => {
    loading.value = true;
    SharedAssetAPI.getAll().then((res: any) => {
      assets.value = res.data;
      loading.value = false;
    });
  };
  
  refresh();

  return { assets, loading, refresh };
}
