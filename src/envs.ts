export const getEnv = (name: string) => {
  return (
    (window && (window as any).configs && (window as any).configs[name]) ||
    import.meta.env[name]
  );
};
