import { setupLayouts } from "layouts-generated";
import { createRouter, createWebHistory } from "vue-router";
import generatedRoutes from "virtual:generated-pages";
import AuthGuard from "@/guards/auth.guard";

const routes = setupLayouts(generatedRoutes);

const router = createRouter({
  routes,
  history: createWebHistory(),
});

router.beforeEach((to, from, next) => {
  AuthGuard(to, from, next);
});

export default router;
