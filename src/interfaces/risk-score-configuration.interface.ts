export interface RiskScoreMapping {
  piiId: number | null;
  datasetAttribute: string;
  privacyLevel: {
    anonymDigits: number;
    totalDigits: number;
  };
}
export interface RiskScoreConfiguration {
  assetId: string | null;
  assetName: string | null;
  useTmp: boolean;
  usePersona: boolean;
  useEncryption: boolean;
  collection: RiskScoreMapping[];
}
