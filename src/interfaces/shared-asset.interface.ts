export interface SharedAsset {
  id: Number;
  name: String;
  description: String;
  keywords: String[];
  assetId: Number;
  configuration: any;
}
