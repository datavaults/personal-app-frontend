import { MessageType, RequestResponseType, SenderType } from "@/constants";

export interface Message {
  id: Number;
  topic: String;
  messageType: MessageType;
  senderType: SenderType;
  timestamp: Date;
  body: String;
  readAt: Date | null;
  configuration?: any;
  responseConfiguration?: any;
  senderUUID?: string;
  senderName?: string;
  senderWebsite?: string;
  senderDescription?: string;
  price?: number | null;
  response: RequestResponseType;
}
