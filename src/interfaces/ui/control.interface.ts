export interface Control {
  label: string;
  click?: string;
  icon?: string;
  size?: string;
  secondary?: boolean;
  colour?: string;
  disabled?: boolean;
}
