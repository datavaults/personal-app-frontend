export interface ProductInBasket {
  idProduct: number;
  amount?: string;
  price?: string;
}

export interface ShoppingBasket {
  customerName: string;
  merchantName: string;
  products?: Array<ProductInBasket>;
}
