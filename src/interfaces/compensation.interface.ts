export interface Compensation {
  id: string;
  name: string;
  description?: string;
  points: string;
  date: string;
  status: string;
}
