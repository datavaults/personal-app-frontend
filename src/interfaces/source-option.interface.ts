export interface SourceOption {
  id: string;
  name: string;
  sourcetype: string;
  url: string;
  auth_type: string;
  auth_properties: string[];
}
