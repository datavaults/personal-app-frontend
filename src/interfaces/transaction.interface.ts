export interface Transaction {
  id: Number;
  buyerName: String;
  buyerUUID: String;
  buyerDescription: String;
  buyerWebsite: String;
  buyerType: String;
  asset: String;
  points: Number;
  type: String;
  timestamp: Date;
  hash: String;
}
