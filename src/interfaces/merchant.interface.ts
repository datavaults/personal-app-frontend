export interface Product {
  id?: number;
  name: string;
  description?: string;
  price: string;
  amount?: number;
  remaining?: string;
  details?: Array<string>;
}

export interface Merchant {
  id?: number;
  name: string;
  description?: string;
  products?: Array<Product>;
}
