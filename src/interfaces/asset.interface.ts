export interface Asset {
  id: number;
  name: string;
  description: string;
  keywords: string[];
  sourceType: string;
  updatedAt: Date;
  firstCollection?: Date;
  sharing: { contract: string } | null;
  schedule: { interval: any; next: any };
  data: any[];
  metadata: any;
  dataSize: number;
  metadataSize: number;
  dataSchemaSize: number;
  uid: string;
  total: number;
}
