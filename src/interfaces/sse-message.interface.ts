import { MessageType, SseQueue } from "../constants";

export interface SseMessage {
  message: any;
  queue: SseQueue;
}

export interface LiveMessage {
  userId: number;
  payload: {
    senderName: string;
    senderType: string;
    messageType: MessageType;
    topic: string;
  };
}
