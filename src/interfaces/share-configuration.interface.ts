import { ConfigurationLevel } from "@/constants/configuration-level.constant";
import { RiskScoreMapping } from "./risk-score-configuration.interface";

export interface ShareConfiguration {
  asset: string | null;
  assetUid: string | null;
  assetUUID: string | null;
  name: string | null;
  description: string | null;
  keywords: string[];
  details: {
    fromExistingConfig: boolean;
    existingConfig: any | null;
    existingName: string | null;
    existingId: string | null;
  };
  configuration: {
    level: ConfigurationLevel;
    anonymisation: {
      anonymise: boolean;
      pseudoId: string | null;
      useTPM: boolean;
      rules: {
        colNames: string[];
        buildIDs: string[];
        anonLevels: string[];
      };
    };
    specificPrice: boolean;
    price: number | null;
    licenseType: string | any;
    standardLicense: string | null;
    ownLicense: string | null;
    encryption: boolean;
    persona: boolean;
    useTPM: boolean;
    tpmPseudonym: null;
    shareType: string;
    accessPolicies: {
      policyId: any;
      existingPolicyId: any;
      sector: any[];
      type: any[];
      size: any[];
      continent: any[];
      country: any[];
      reputation: any;
    };
  };
  fieldMapping: RiskScoreMapping[];
}
