export interface Question {
  id: number;
  question: string;
  description: string | null;
  type:
    | "radio"
    | "checkbox"
    | "text"
    | "number"
    | "textarea"
    | "dropdown"
    | "tags";
  options: any[];
  placeholder: string | null;
  default_value: any[];
  required: boolean;
  other: boolean;
  multiple: boolean;
  selectionsRange: { min: number | null; max: number | null };
  valueRange: { min: number | null; max: number | null };
  answer: any[];
}

export interface Questionnaire {
  id: number;
  creationDate: Date;
  lastUpdateDate: Date;
  expirationDate: Date | null;
  dataSeekerOrganization: string;
  dataSeekerURL: string;
  status: string;
  title: string;
  intro: string;
  questions: Question[];
}
