export const occupations = [
  {
    id: 1,
    value: "Student",
    label: 'constants.occupations.student',
  },
  {
    id: 2,
    value: "Full-time Employee",
    label: 'constants.occupations.fullTime',
  },
  {
    id: 3,
    value: "Part-Time Employee",
    label: 'constants.occupations.partTime',
  },
  {
    id: 4,
    value: "Business Owner/Self Employed",
    label: 'constants.occupations.owner',
  },
  {
    id: 5,
    value: "Unemployed",
    label: 'constants.occupations.unemployed',
  },
];
