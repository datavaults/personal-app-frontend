import { CollectionFrequencyEnum } from "@/constants/collection-frequency.constant";

export class CollectionFrequencyWrapper {
  static readonly ONCE = new CollectionFrequencyWrapper(
    CollectionFrequencyEnum.Once,
    null
  );

  static readonly MINUTE = new CollectionFrequencyWrapper(
    CollectionFrequencyEnum.Minute,
    "MINUTE"
  );

  static readonly HOUR = new CollectionFrequencyWrapper(
    CollectionFrequencyEnum.Hour,
    "HOUR"
  );

  static readonly DAY = new CollectionFrequencyWrapper(
    CollectionFrequencyEnum.Day,
    "DAY"
  );

  static readonly WEEK = new CollectionFrequencyWrapper(
    CollectionFrequencyEnum.Week,
    "WEEK"
  );

  static readonly MONTH = new CollectionFrequencyWrapper(
    CollectionFrequencyEnum.Month,
    "MONTH"
  );

  private label: CollectionFrequencyEnum;
  private value: string | null;

  constructor(label: CollectionFrequencyEnum, value: string | null) {
    this.label = label;
    this.value = value;
  }

  public simpleObject(): {
    value: string | null;
    label: CollectionFrequencyEnum;
  } {
    return {
      value: this.value,
      label: this.label,
    };
  }

  public getValue(): string | null {
    return this.value;
  }

  public static all() {
    return [
      CollectionFrequencyWrapper.ONCE,
      CollectionFrequencyWrapper.MINUTE,
      CollectionFrequencyWrapper.HOUR,
      CollectionFrequencyWrapper.DAY,
      CollectionFrequencyWrapper.WEEK,
      CollectionFrequencyWrapper.MONTH,
    ];
  }

  public static findByLabel(
    label: CollectionFrequencyEnum | null
  ): CollectionFrequencyWrapper | undefined {
    return CollectionFrequencyWrapper.all().find(
      (wrapper: CollectionFrequencyWrapper) => wrapper.label === label
    );
  }

  public static findByValue(
    value: string | null
  ): CollectionFrequencyWrapper | null {
    const findResult = CollectionFrequencyWrapper.all().find(
      (wrapper: CollectionFrequencyWrapper) => wrapper.value === value
    );
    return findResult || null;
  }

  public static getLabel(value: string | null): CollectionFrequencyEnum | null {
    const wrapper: CollectionFrequencyWrapper | null = this.findByValue(value);
    return wrapper ? wrapper.label : null;
  }

  public static getValue(label: CollectionFrequencyEnum | null): string | null {
    const wrapper: CollectionFrequencyWrapper | undefined =
      this.findByLabel(label);
    return wrapper ? wrapper.value : null;
  }
}
