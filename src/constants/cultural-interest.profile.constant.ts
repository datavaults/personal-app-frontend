export const culture = [
  {
    id: 1,
    value: "Visual Arts",
    label: "constants.culture.visual",
  },
  {
    id: 2,
    value: "Performing Arts",
    label: "constants.culture.performing",
  },
  {
    id: 3,
    value: "Plastic Arts",
    label: "constants.culture.plastic",
  },
  {
    id: 4,
    value: "Music",
    label: "constants.culture.music",
  },
  {
    id: 5,
    value: "Fashion",
    label: "constants.culture.fashion",
  },
  {
    id: 6,
    value: "Architecture",
    label: "constants.culture.architecture",
  },
  {
    id: 7,
    value: "History",
    label: "constants.culture.history",
  },
  {
    id: 8,
    value: "I do not want to answer",
    label: "constants.culture.noAnswer",
  },
];
