export const transportation = [
    {
        "id": 1,
        "value": "On foot",
        "label": 'constants.transportation.onFoot'
    },
    {
        "id": 2,
        "value": "Bicycle",
        "label": 'constants.transportation.bicycle'
    },
    {
        "id": 3,
        "value": "Motorcycle",
        "label": 'constants.transportation.motorcycle'
    },
    {
        "id": 4,
        "value": "Car",
        "label": 'constants.transportation.car'
    },
    {
        "id": 5,
        "value": "Taxi",
        "label": 'constants.transportation.taxi'
    },
    {
        "id": 6,
        "value": "Public Bus",
        "label": 'constants.transportation.bus'
    },
    {
        "id": 7,
        "value": "Tram",
        "label": 'constants.transportation.tram'
    },
    {
        "id": 8,
        "value": "Subway",
        "label": 'constants.transportation.subway'
    },
    {
        "id": 9,
        "value": "I do not want to answer",
        "label": 'constants.transportation.noAnswer'
    }
];
