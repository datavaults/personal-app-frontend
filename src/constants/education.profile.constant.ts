export const education = [
  {
    id: 1,
    value: "High school",
    label: "constants.education.highSchool",
  },
  {
    id: 2,
    value: "Bachelor Degree",
    label: "constants.education.bachelor",
  },
  {
    id: 3,
    value: "Master`s degree",
    label: "constants.education.masters",
  },
  {
    id: 4,
    value: "Doctor of Philosophy",
    label: "constants.education.doctorate",
  },
  {
    id: 5,
    value: "I do not want to answer",
    label: "constants.education.noAnswer",
  },
];
