export const disabilities = [
  {
    id: 1,
    value: "Acquired brain injury",
    label: "constants.disabilities.brainInjury",
  },
  {
    id: 2,
    value: "Anxiety disorders and stress",
    label: "constants.disabilities.anxiety",
  },
  {
    id: 3,
    value: "Autism spectrum disorder",
    label: "constants.disabilities.autism",
  },
  {
    id: 4,
    value: "Bipolar disorder",
    label: "constants.disabilities.bipolar",
  },
  {
    id: 5,
    value: "Cancer",
    label: "constants.disabilities.cancer",
  },
  {
    id: 6,
    value: "Chronic pain",
    label: "constants.disabilities.pain",
  },
  {
    id: 7,
    value: "Dementia",
    label: "constants.disabilities.dementia",
  },
  {
    id: 8,
    value: "Depression",
    label: "constants.disabilities.depression",
  },
  {
    id: 9,
    value: "Diabetes",
    label: "constants.disabilities.diabetes",
  },
  {
    id: 10,
    value: "Disability etiquette",
    label: "constants.disabilities.etiquette",
  },
  {
    id: 11,
    value: "Dyslexia",
    label: "constants.disabilities.dyslexia",
  },
  {
    id: 12,
    value: "Dyspraxia",
    label: "constants.disabilities.dyspraxia",
  },
  {
    id: 13,
    value: "Epilepsy",
    label: "constants.disabilities.epilepsy",
  },
  {
    id: 14,
    value: "Hearing Impairment (deafness)",
    label: "constants.disabilities.deafness",
  },
  {
    id: 15,
    value: "Learning disability",
    label: "constants.disabilities.learning",
  },
  {
    id: 16,
    value: "Limb loss",
    label: "constants.disabilities.limb",
  },
  {
    id: 17,
    value: "Mental health",
    label: "constants.disabilities.mental",
  },
  {
    id: 18,
    value: "Migraine",
    label: "constants.disabilities.migraine",
  },
  {
    id: 19,
    value: "Multiple sclerosis",
    label: "constants.disabilities.sclerosis",
  },
  {
    id: 20,
    value: "Musculoskeletal disorders",
    label: "constants.disabilities.muscular",
  },
  {
    id: 21,
    value: "Visual impairment (blindness).",
    label: "constants.disabilities.blindness",
  },
  {
    id: 22,
    value: "Schizophrenia",
    label: "constants.disabilities.schizo",
  },
  {
    id: 23,
    value: "Non Disabled",
    label: "constants.disabilities.non",
  },
  {
    id: 24,
    value: "I do not want to answer",
    label: "constants.disabilities.noAnswer",
  },
];
