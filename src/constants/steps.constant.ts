export class Step {
  static readonly SELECT_DATA_ASSET = new Step("share", "components.sharingHeader.selectDataAsset");
  static readonly ANONYMISATION = new Step("share-Anonymisation", "components.sharingHeader.anonymisation");
  static readonly ACCESS_POLICY = new Step("share-AccessPolicy", "components.sharingHeader.accessPolicySelection");
  static readonly FIELD_MAPPING = new Step("share-FieldMapping", "components.sharingHeader.fieldMapping");
  static readonly OTHER_INFORMATION = new Step(
    "share-OtherInformation",
    "components.sharingHeader.otherSharingInformation"
  );
  static readonly REVIEW_EXECUTE = new Step("share-ReviewExecute", "components.sharingHeader.reviewAndExecute");
  static readonly PROGRESS = new Step("share-Progress", "components.sharingHeader.shareAsset");

  private readonly pageName: string;
  readonly labelKey: string;

  constructor(pageName: string, labelKey: string) {
    this.pageName = pageName;
    this.labelKey = labelKey;
  }

  public getPageName(): string {
    return this.pageName;
  }

  public static all() {
    return [
      this.SELECT_DATA_ASSET,
      this.ANONYMISATION,
      this.ACCESS_POLICY,
      this.FIELD_MAPPING,
      this.OTHER_INFORMATION,
      this.REVIEW_EXECUTE,
      this.PROGRESS,
    ];
  }

  public static findByPosition(position: number) {
    return this.all()[position];
  }

  public static findPositionByName(name: string) {
    return (
      this.all()
        .map((step: Step) => step.pageName)
        .indexOf(name) + 1
    );
  }
}
