export enum SourcesEnum {
  GOOGLEFIT = "Google Fit",
  ROUTE = "Route",
  LOCATION = "Location",
  QUESTIONNAIRE = "Questionnaire",
}
