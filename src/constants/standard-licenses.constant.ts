export const standardLicenses = [
  {
    label: "CC-0",
    value: "CC0",
    description: 'constants.licenses.cc0Description',
  },
  {
    label: "CC-BY",
    value: "CC_BY",
    description:  'constants.licenses.ccBYDescription',
  },
  {
    label: "CC-BY-NC-ND",
    value: "CC_BYNCND",
    description:
    'constants.licenses.ccBYNCNDDescription',
  },
  {
    label: 'constants.licenses.allRightsReserved',
    value: "all_rights_reserved",
    description: 'constants.licenses.allRightsReservedDescription',
  },
  //older / previous licenses
  // {
  //   label: "CC-BY-SA",
  //   value: "CC_BY_SA",
  //   description:
  //     "By Attribution, with a Share-Alike clause which means that anyone sharing or modifying the original work must release it under the same license.",
  // },
  // {
  //   label: "CC-BY-ND",
  //   value: "CC_BY_ND",
  //   description:
  //     "The No Derivatives clause prevents anyone from producing a modified version of a work, whilst fully allowing sharing.",
  // },
  // {
  //   label: "CC-BY-NC",
  //   value: "CC_BY_NC",
  //   description:
  //     "The Non Commercial clause specifically prevents reuse of material for commercial purposes.",
  // },
  // {
  //   label: "CC-BY-NC-SA",
  //   value: "CC_BY_NC_SA",
  //   description:
  //     "Non Commerical and Share-Alike clauses ensure any derivative material must also be shared under a Non-Commercial license.",
  // },
];
