export const Europe = [
  {
    label: "constants.countries.europe.austria",
    value: "Austria",
  },
  {
    label: "constants.countries.europe.belgium",
    value: "Belgium",
  },
  {
    label: "constants.countries.europe.cyprus",
    value: "Cyprus",
  },
  {
    label: "constants.countries.europe.denmark",
    value: "Denmark",
  },
  {
    label: "constants.countries.europe.france",
    value: "France",
  },
  {
    label: "constants.countries.europe.germany",
    value: "Germany",
  },
  {
    label: "constants.countries.europe.greece",
    value: "Greece",
  },
  {
    label: "constants.countries.europe.ireland",
    value: "Ireland",
  },
  {
    label: "constants.countries.europe.italy",
    value: "Italy",
  },
  {
    label: "constants.countries.europe.spain",
    value: "Spain",
  },
  {
    label: "constants.countries.europe.unitedKingdom",
    value: "United Kingdom",
  },
];

export const Asia = [
  {
    label: "constants.countries.asia.china",
    value: "China",
  },
  {
    label: "constants.countries.asia.india",
    value: "India",
  },
  {
    label: "constants.countries.asia.israel",
    value: "Israel",
  },
  {
    label: "constants.countries.asia.japan",
    value: "Japan",
  },
  {
    label: "constants.countries.asia.southKorea",
    value: "South Korea",
  },
  {
    label: "constants.countries.asia.turkey",
    value: "Turkey",
  },
  {
    label: "constants.countries.asia.uae",
    value: "United Arab Emirates",
  },
];

export const America = [
  {
    label: "constants.countries.america.argentina",
    value: "Argentina",
  },
  {
    label: "constants.countries.america.bolivia",
    value: "Bolivia",
  },
  {
    label: "constants.countries.america.brazil",
    value: "Brazil",
  },
  {
    label: "constants.countries.america.canada",
    value: "Canada",
  },
  {
    label: "constants.countries.america.chile",
    value: "Chile",
  },
  {
    label: "constants.countries.america.colombia",
    value: "Colombia",
  },
  {
    label: "constants.countries.america.mexico",
    value: "Mexico",
  },
  {
    label: "constants.countries.america.usa",
    value: "United States",
  },
  {
    label: "constants.countries.america.venezuela",
    value: "Venezuela",
  },
];

export const Africa = [
  {
    label: "constants.countries.africa.algeria",
    value: "Algeria",
  },
  {
    label: "constants.countries.africa.ethiopia",
    value: "Ethiopia",
  },
  {
    label: "constants.countries.africa.egypt",
    value: "Egypt",
  },
  {
    label: "constants.countries.africa.kenya",
    value: "Kenya",
  },
  {
    label: "constants.countries.africa.morocco",
    value: "Morocco",
  },
  {
    label: "constants.countries.africa.southAfrica",
    value: "South Africa ",
  },
  {
    label: "constants.countries.africa.tanzania",
    value: "Tanzania",
  },
];

export const Oceania = [
  {
    label: "constants.countries.oceania.australia",
    value: "Australia",
  },
  {
    label: "constants.countries.oceania.newZealand",
    value: "New Zealand",
  },
];
