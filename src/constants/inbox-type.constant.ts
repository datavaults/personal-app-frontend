export enum InboxType {
  Incoming = "Incoming",
  Outgoing = "Outgoing",
}
