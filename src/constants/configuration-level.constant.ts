export enum ConfigurationLevel {
  MAX = "max",
  LOW = "low",
  CUSTOM = "custom",
  EXISTING = "existing"
}