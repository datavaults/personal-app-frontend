import { AnonymisationHierarchyGroup } from "@/constants/anonymisation-hierarchy-group.constant";
import { NAMESPACE } from "@/constants/namespace.constant";

export class ColumnType {
  static readonly STRING = new ColumnType(
    `http://www.w3.org/2001/XMLSchema#string`,
    AnonymisationHierarchyGroup.String
  );

  static readonly DATETIME = new ColumnType(
    `http://www.w3.org/2001/XMLSchema#dateTime`,
    AnonymisationHierarchyGroup.String
  );

  static readonly LINK = new ColumnType(
    `http://www.w3.org/2001/XMLSchema#anyURL`,
    AnonymisationHierarchyGroup.String
  );

  static readonly INTEGER = new ColumnType(
    `http://www.w3.org/2001/XMLSchema#integer`,
    AnonymisationHierarchyGroup.Number
  );

  static readonly DATABASEKEY = new ColumnType(
    `${NAMESPACE}/dv#DatabaseKey`,
    AnonymisationHierarchyGroup.String
  );

  static readonly FREETEXT = new ColumnType(
    `${NAMESPACE}/dv#FreeText`,
    AnonymisationHierarchyGroup.String
  );

  private iri: string;
  private anonymisationType: AnonymisationHierarchyGroup;

  constructor(iri: string, anonymisationType: AnonymisationHierarchyGroup) {
    this.iri = iri;
    this.anonymisationType = anonymisationType;
  }

  public static all() {
    return [
      this.STRING,
      this.DATETIME,
      this.LINK,
      this.INTEGER,
      this.DATABASEKEY,
      this.FREETEXT,
    ];
  }

  public static findByIri(iri: string) {
    return this.all().find((columnType: ColumnType) => columnType.iri === iri);
  }
}
