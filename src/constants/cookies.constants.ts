export enum Cookies {
  SHARE_WARNING_COOKIE_DISMISSED = "shareWarningCookieDismised",
  LANGUAGE = "language",
  NATIONAL_INSURANCE = "nationalInsurance"
}
