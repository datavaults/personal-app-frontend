export enum CollectionFrequencyEnum {
  Once = "collection.once",
  Minute = "collection.minute",
  Hour = "collection.hourly",
  Day = "collection.daily",
  Week = "collection.weekly",
  Month = "collection.monthly",
}


//below seems not to be used by anything so not translated
export const CollectionFrequency: Record<
  string,
  { value: CollectionFrequencyEnum; message?: string }
> = {
  once: { value: CollectionFrequencyEnum.Once },
  minute: {
    value: CollectionFrequencyEnum.Minute,
    message: "fetched every minute",
  },
  hour: {
    value: CollectionFrequencyEnum.Hour,
    message: "fetched every hour at the same minute",
  },
  day: {
    value: CollectionFrequencyEnum.Day,
    message: "fetched every day at the same time",
  },
  week: {
    value: CollectionFrequencyEnum.Week,
    message: "fetched every week at the same time",
  },

  month: {
    value: CollectionFrequencyEnum.Month,
    message: "fetched every month at the same hour",
  },
};
