import { MessageType } from "@/constants/message-type.constant";
import { MessageQuestionnaire, SharingRequest } from "@/components/messages";
import { LiveMessage } from "@/interfaces";

export class MessageTypeWrapper {
  static readonly SHARING_REQUEST = new MessageTypeWrapper(
    MessageType.SharingRequest,
    (data: any) =>
      `${data.senderType} ${data.senderName} send you a new sharing request`,
    true,
    SharingRequest
  );
  static readonly QUESTIONNAIRE = new MessageTypeWrapper(
    MessageType.Questionnaire,
    (data: any) =>
      `${data.senderType} ${data.senderName} send you a new questionnaire`,
    true,
    MessageQuestionnaire
  );
  static readonly SYSTEM_NOTIFICATION = new MessageTypeWrapper(
    MessageType.SystemNotification,
    (data: any) => `System notification: ${data.topic}`
  );
  static readonly TRANSACTION_RESULT = new MessageTypeWrapper(
    MessageType.TransactionResult,
    (data: any) => `Transaction update: ${data.topic}`
  );

  private messageType: MessageType;
  private component: any;
  private request: boolean;
  private notificationMessage: Function;

  constructor(
    messageType: MessageType,
    notificationMessage: Function,
    request: boolean = false,
    component: any = null
  ) {
    this.messageType = messageType;
    this.component = component;
    this.request = request;
    this.notificationMessage = notificationMessage;
  }

  public getComponent() {
    return this.component;
  }

  public isRequest() {
    return this.request;
  }

  public getNotification(message: LiveMessage) {
    return this.notificationMessage(message.payload);
  }

  public static all() {
    return [
      MessageTypeWrapper.SHARING_REQUEST,
      MessageTypeWrapper.QUESTIONNAIRE,
      MessageTypeWrapper.SYSTEM_NOTIFICATION,
      MessageTypeWrapper.TRANSACTION_RESULT,
    ];
  }

  public static find(messageType: MessageType): MessageTypeWrapper | undefined {
    return MessageTypeWrapper.all().find(
      (typeWrapper: MessageTypeWrapper) =>
        typeWrapper.messageType === messageType
    );
  }
}
