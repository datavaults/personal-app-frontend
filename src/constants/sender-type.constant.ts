export enum SenderType {
  System = "System",
  DataSeeker = "Data Seeker",
  DataOwner = "Data Owner",
}
