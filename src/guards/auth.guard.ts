import { User } from "@/api";
import { useAuthentication, useDemonstrators } from "@/composables";
import store from "@/store";
import { NavigationGuard } from "vue-router";

type Next = Parameters<NavigationGuard>[2];
const { login } = useAuthentication();

const AuthGuard = async (to: any, from: any, next: Next) => {
  const { shouldGoToTerms } = useDemonstrators();

  if (document.location.hash.length > 0 && document.location.hash.match(/^#.*code=[a-zA-Z0-9]*/g)) {
    await login(to.name);
  }

  // if should go to terms and not going to terms already then go
  if (store.getters.isAuthenticated && shouldGoToTerms.value && to.name !== "terms") {
    return next("/terms");
  }

  // if should not go to terms and  going to terms already then go to home page
  if (store.getters.isAuthenticated && !shouldGoToTerms.value && to.name === "terms") {
    return next("/dashboard");
  }

  if (store.getters.isAuthenticated || to.meta.isPublic) {
    store.commit("SET_IS_READY");
    return next();
  }

  User.profile()
    .then(({ data }) => {
      store.commit("SET_USER", data);
      store.commit("SET_IS_READY");

      if (shouldGoToTerms.value) {
        return next("/terms");
      }

      return next(to);
    })
    .catch(async () => {
      store.commit("SET_IS_READY");

      await login();
    });
};

export default AuthGuard;
