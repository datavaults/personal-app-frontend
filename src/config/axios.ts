import axios from "axios";
import store from "../store";
import router from "../router";
import { useAuthentication, useToaster } from "@/composables";
import { v4 as uuidv4 } from "uuid";

const baseURL = "/api/";
const toaster = useToaster();
let cancelTokens: any = {};
const { login } = useAuthentication();

function cancelAllRequests(error: any) {
  cancelTokens = Object.keys(cancelTokens).reduce((acc: any, cancelTokenUid: string) => {
    cancelTokens[cancelTokenUid].cancel(error);
    return acc;
  }, {});
}

function removeSpecificCancelToken(tokenUidToRemove: any) {
  cancelTokens = Object.keys(cancelTokens).reduce((acc: any, cancelTokenUid: string) => {
    if (tokenUidToRemove !== cancelTokenUid) {
      acc[cancelTokenUid] = cancelTokens[cancelTokenUid];
    }
    return acc;
  }, {});
}

const secured = axios.create({
  baseURL,
  withCredentials: true,
  headers: {
    "Content-Type": "application/json",
    "Cache-Control": "no-cache",
  },
});

const blobSecured = axios.create({
  baseURL,
  withCredentials: true,
  headers: {
    "Content-Type": "application/json",
    "Cache-Control": "no-cache",
  },
  responseType: "arraybuffer",
});

const plain = axios.create({
  baseURL,
  withCredentials: true,
  headers: {
    "Content-Type": "application/json",
  },
});

secured.interceptors.request.use(async config => {
  const token = store.getters.token;
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  const cancelTokenUid = uuidv4();
  cancelTokens[cancelTokenUid] = axios.CancelToken.source();
  return {
    ...config,
    cancelTokenUid: cancelTokenUid,
    cancelToken: cancelTokens[cancelTokenUid].token,
  };
});

// TODO valanto - 04/08/2021 - Put interceptor back to redirect user
// in case their token expires while app is running
secured.interceptors.response.use(
  (response: any) => {
    if (response?.config?.cancelTokenUid) removeSpecificCancelToken(response.config.cancelTokenUid);
    return response;
  },
  async error => {
    if (error?.config?.cancelTokenUid) removeSpecificCancelToken(error.config.cancelTokenUid);
    if (error?.response?.status === 401) {
      if (store.getters.isAuthenticated) {
        store.commit("CLEAR_USER");
        toaster.show("Your authenticated session has expired. You need to login again", "warning");
      }

      cancelAllRequests(error);

      await login();
      return;
    }
    return error;
  }
);


const handleErrorResponse = async (error: any) => {
  if (error.response && error.response.status === 500) {
    await router.push({ name: "error-500" });
  }
  return Promise.reject(error);
};

plain.interceptors.response.use(null as any, handleErrorResponse);
secured.interceptors.response.use(null as any, handleErrorResponse);

export { plain, secured, blobSecured };
