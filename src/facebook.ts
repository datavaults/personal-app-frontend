import { getEnv } from "./envs";

const facebookAppId = getEnv("VITE_FACEBOOK_APP_ID");

export function initFacebookSdk() {
  return new Promise<void>(resolve => {
    (function (d, s, id) {
      let js: any,
        fjs: any = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    })(document, "script", "facebook-jssdk");

    // @ts-ignore
    window.fbAsyncInit = function () {
      FB.init({
        //@ts-ignore
        appId: facebookAppId,
        cookie: true,
        xfbml: true,
        version: "v11.0",
      });

      FB.AppEvents.logPageView();
      resolve();
    };
  });
}
