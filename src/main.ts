import { Toaster } from "@/components/toaster";
import "@vueform/multiselect/themes/default.css";
import axios from "axios";
import VCalendar from "v-calendar";
import { Tooltip } from "v-tooltip";
import "v-tooltip/dist/v-tooltip.css";
import { configure } from "vee-validate";
import { createApp } from "vue";
import VueAxios from "vue-axios";
import { createI18n } from "vue-i18n";
import App from "./App.vue";
import { initFacebookSdk } from "./facebook";
import "./index.css";
import router from "./router";
import store from "./store";
import "./utils";
import validationMessages from "./validation-messages";
import { useLocale } from "./messages/all";
import messages from "@intlify/vite-plugin-vue-i18n/messages";
import VueMarkdownEditor from "@kangc/v-md-editor";
import "@kangc/v-md-editor/lib/style/base-editor.css";
import vuepressTheme from "@kangc/v-md-editor/lib/theme/vuepress.js";
import "@kangc/v-md-editor/lib/theme/style/vuepress.css";
import enUS from "@kangc/v-md-editor/lib/lang/en-US";
import VMdPreview from "@kangc/v-md-editor/lib/preview";
import "@kangc/v-md-editor/lib/style/preview.css";

import Prism from "prismjs";
import { getEnv } from "../envs";

VueMarkdownEditor.use(vuepressTheme, {
  Prism,
});
VueMarkdownEditor.lang.use("en-US", enUS);
VMdPreview.use(vuepressTheme, {
  Prism,
});

const app = createApp(App);

configure({
  validateOnInput: true,
  generateMessage: context => validationMessages(context),
});

const { defaultLocale } = useLocale();

const i18n = createI18n({
  legacy: false,
  globalInjection: true,
  locale: defaultLocale.code,
  fallbackLocale: defaultLocale.code,
  messages,
});

app.use(router);
app.use(store);
app.use(VueAxios, axios);
app.use(VCalendar, {});
app.use(i18n);
app.use(VueMarkdownEditor);
app.use(VMdPreview);

// Global components
app.component("Toaster", Toaster);
app.component("VTooltip", Tooltip);
if (getEnv("VITE_STARTER_KIT_DEMO") !== "true") initFacebookSdk().then(() => app.mount("#app"));
else app.mount("#app");
//TODO: Remove if possible
//used by composables/tpm.ts, composables/messages.ts, validation-messages.ts, utils/rules.ts
export default i18n;
