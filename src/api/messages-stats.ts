import { secured } from "../config/axios";

const resource = "message-stats";
const questionnairesResource = "questionnaires";

export default {
  getQuestionnaires: (year: string) => secured.get(`${resource}/${questionnairesResource}/${year}`),
  getSoldQuestionnaires: () => secured.get(`${resource}/${questionnairesResource}/`),
};
