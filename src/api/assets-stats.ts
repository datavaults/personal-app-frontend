import { secured } from "../config/axios";

const resource = "assets-stats";
const yearResource = "years";
const totalAssetsResource = "totalassets";
const totalSharedAssetsResource = "totalsharedassets";
const sharedAssetResource = "shared-asset";

export default {
  getYears: () => secured.get(`${resource}/${yearResource}`),
  totalAssets: (start: string, end: string) => secured.get(`${resource}/${totalAssetsResource}/${start}/${end}`),
  totalSharedAssets: (start: string, end: string) => secured.get(`${resource}/${totalSharedAssetsResource}/${start}/${end}`),
  sharedAsset: (year: string) => secured.get(`${resource}/${sharedAssetResource}/${year}`),
  cardsTotalSharedAssets: () => secured.get(`${resource}/${totalSharedAssetsResource}`)
};
