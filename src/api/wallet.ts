import { ShoppingBasket } from "@/interfaces";
import { secured } from "../config/axios";

const resource = "wallet";

export default {
  createAccount: (accountId: string, customer: any) => secured.post(`/${resource}/${accountId}`, customer),
  getCompensations: (accountId: string) => secured.get(`/${resource}/compensations/${accountId}`),
  getMerchants: () => secured.get(`/${resource}/merchants/`),
  getMerchant: (id: string | number) => secured.get(`/${resource}/merchant/${id}`),
  purchase: (shoppingBasket: ShoppingBasket) => secured.post(`/${resource}/purchase`, shoppingBasket),
  useCompensation: (compensationId: string) => secured.post(`/${resource}/compensations/use/${compensationId}`),
  invoices: () => secured.get(`/${resource}/invoices`),
};
