import { secured } from "../config/axios";

const resource = "sharing-configuration";

export default {
  all: () => secured.get(`${resource}/all/`),
  get: (id: number) => secured.get(`${resource}/${id}/`),
  create: (data: any) => secured.post(`${resource}/`, data),
  update: (id: number, data: any) => secured.put(`${resource}/${id}/`, data),
  delete: (id: number) => secured.delete(`${resource}/${id}/`),
};
