import { plain, secured } from "../config/axios";

const resource = "/auth";

export default {
  keycloakLogin: (
    userId: string | undefined,
    userToken: string | undefined,
    userRefreshToken: string | undefined
  ) =>
    plain.post(`${resource}/keycloakLogin`, {
      profile: userId,
      accessToken: userToken,
      refreshToken: userRefreshToken,
    }),
  login: (data: any) => plain.post(`${resource}/login`, data),
  register: (data: any) => plain.post(`${resource}/register`, data),
  logout: () => secured.post(`${resource}/logout`),
  changePassword: (data: any) =>
    secured.put(`${resource}/changepassword`, data),
  resetPassword: (data: any) => plain.put(`${resource}/resetpassword`, data),
  verifyResetPassword: (data: any) =>
    plain.post(`${resource}/verifyResetPassword/`, data),
  verifyEmail: (data: any) => plain.get(`${resource}/verifyEmail/${data}`),
};
