import { secured } from "../config/axios";

const resource = "access-policy-template";

export default {
  all: () => secured.get(`${resource}/all/`),
  get: (id: number) => secured.get(`${resource}/${id}/`),
  create: (data: any) => secured.post(`${resource}/`, data),
  update: (id: number, data: any) => secured.patch(`${resource}/${id}/`, data),
};
