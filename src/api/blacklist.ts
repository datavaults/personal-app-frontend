import { secured } from "../config/axios";

const resource = "blacklist";

export default {
  get: () => secured.get(`${resource}`),
  addToBlacklist: (data: any) => secured.post(`${resource}`, data),
  removeFromBlacklist: (uuid: string) => secured.delete(`${resource}/${uuid}`),
};
