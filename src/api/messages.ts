import { secured } from "@/config/axios";

const resource = "message";

export default {
  getAll: () => secured.get(`${resource}`),
  get: (id: number) => secured.get(`${resource}/${id}`),
  markAsRead: (id: number) => secured.put(`${resource}/${id}/markAsRead`),
  accept: (id: number, data: any = {}) =>
    secured.put(`${resource}/${id}/accept`, data),
  decline: (id: number) => secured.put(`${resource}/${id}/decline`),
  declineAndBlock: (id: number) =>
    secured.put(`${resource}/${id}/declineAndBlock`),
  delete: (id: number) => secured.delete(`${resource}/${id}`),
};
