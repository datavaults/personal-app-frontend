import { blobSecured } from "../config/axios";

const resource = "download/";
const assets = "assets";
const asset = "asset";

export default {
  downloadAllData: () => blobSecured.get(`${resource}${assets}/all`),
  downloadSpecificData: (ids: number[]) => blobSecured.get(`${resource}${assets}/specific`, { params: { ids: ids } }),
  downloadById : (id: number) => blobSecured.get(`${resource}${asset}/${id}`),
};
