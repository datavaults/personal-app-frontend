import { secured } from "../config/axios";

const resource = "data-fetcher";
const sourceResource = "asset";
const recentUpdateresource = "recent-update";

export default {
  getSourceOptions: () => secured.get(`${resource}/source-options`),
  createAsset: (data: any) => secured.post(`${resource}/${sourceResource}`, data),
  getAssets: () => secured.get(`${resource}/${sourceResource}`),
  getAsset: (id: number) => secured.get(`${resource}/${sourceResource}/${id}`),
  downloadData: (id: number) => secured.get(`${resource}/${sourceResource}/${id}/data`),
  downloadMetadata: (id: number) => secured.get(`${resource}/${sourceResource}/${id}/metadata`),
  downloadDataSchema: (id: number) => secured.get(`${resource}/${sourceResource}/${id}/data-schema`),
  updateAsset: (id: number, data: any) => secured.put(`${resource}/${sourceResource}/${id}`, data),
  deleteAsset: (id: number) => secured.delete(`${resource}/${sourceResource}/${id}`),
  createRecentUpdate: (data: any) => secured.post(`${resource}/${recentUpdateresource}`, data),
  getRecentUpdate: (id: number) => secured.get(`${resource}/${recentUpdateresource}/${id}`),
  getRecentUpdatesByAssetId: (assetId: number) =>
    secured.get(`${resource}/${recentUpdateresource}/asset-id/${assetId}`),
  updateRecentUpdate: (id: number, data: any) => secured.put(`${resource}/${recentUpdateresource}/${id}`, data),
  deleteRecentUpdate: (id: number) => secured.delete(`${resource}/${recentUpdateresource}/${id}`),
  updateStatus: (id: number, status: boolean) => secured.patch(`${resource}/status/${status}/${id}`),
};
