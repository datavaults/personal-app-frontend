import { secured } from "../config/axios";

const resource = "transaction";

export default {
  getAll: () => secured.get(`${resource}/all`),
  get: (id: number) => secured.get(`${resource}/${id}`),
  getPoints: () => secured.get(`${resource}/points`),
};
