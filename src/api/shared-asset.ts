import { secured } from "../config/axios";

const resource = "shared-asset";

export default {
  getAll: () => secured.get(`${resource}/all`),
  get: (id: string) => secured.get(`${resource}/${id}`),
  create: (data: any) => secured.post(`${resource}`, data),
};
