import { secured } from "../config/axios";

const resource = "email";

export default {
  send: (data: any) => secured.post(`${resource}/send`, data),
};
