import { secured } from "../config/axios";

const resource = "miwenergia";
const loginResource = "login";
const listResource = "list";

export default {
  login: (data: any) => secured.post(`${resource}/${loginResource}`, data),
  retrieveUspcList: (data: any) => secured.post(`${resource}/${listResource}`, data)
};
