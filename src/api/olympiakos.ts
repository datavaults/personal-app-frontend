import { secured } from "../config/axios";

const resource = "olympiakos";
const checkResource = "checkUser";

export default {
  confirmUser: (id: any, date: any) => secured.get(`${resource}/${checkResource}/${id}/${date}`)
};
