import { secured } from "../config/axios";

const resource = "occupation";

export default {
  all: () =>
    secured.get(`${resource}/all`, {
      headers: {
        "Content-Type": "application/json",
        "Cache-Control": "max-age=86400",
      },
    }),
  get: (id: number) => secured.get(`${resource}/${id}`),
};
