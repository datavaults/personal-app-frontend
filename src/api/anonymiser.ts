import { secured } from "@/config/axios";

const resource = "/anonymiser";

export default {
  getPseudoIds: () => secured.get(`${resource}/pseudoIds`),
  getHierarchiesByType: () => secured.get(`${resource}/hierarchies`),
  getPreview: (data: any) => secured.post(`${resource}/preview`, data),
};
