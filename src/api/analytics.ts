import { secured } from "../config/axios";

const resource = "analytics";

export default {
  getAnalysisMethods: () => secured.get(`${resource}/analysis-methods`),
  analyse: (data: any) => secured.post(`${resource}`, data),
  saveAsset: (data: any) => secured.post(`${resource}/save`, data)
};
