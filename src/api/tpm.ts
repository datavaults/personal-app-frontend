import axios from "axios";
import { v4 as uuidv4 } from "uuid";
import { getEnv } from "../envs";

const daa = "join";
const readPseudonyms = "pseudonyms";
const createPseudonym = "generatepseudonym";
const signData = "sign";
const tpmURL = "https://127.0.0.1:5050/DAA/";

export default {
  joinTPMDAA: async () => {
    return await axios.post(`${tpmURL}${daa}`, {
      headers: {
        accept: "application/json",
      },
    });
  },
  readPseudonymsFromTPM: async () => {
    return await axios
      .get(`${tpmURL}${readPseudonyms}`, {
        headers: {
          accept: "application/json",
        },
      })
      .then(async res => {
        return res.data;
      })
      .catch(error => {
        return { error: error, false: false };
      });
  },
  createTPMPseudonym: async () => {
    return await axios
      .post(`${tpmURL}${createPseudonym}`, {
        headers: {
          accept: "application/json",
        },
      })
      .then(async resp => {
        const newName = uuidv4();
        return {
          label: newName,
          value: newName,
          description: "New pseudonym",
        };
      });
  },
  signHash: async (pseudonym: any, data: any) => {
    return await axios
      .post(`${tpmURL}${signData}/${pseudonym}`, { message: data })
      .then(async res => {
        return res.data;
      })
      .catch(error => {
        return { error: error, false: false };
      });
  },
};
