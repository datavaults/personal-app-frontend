import { secured } from "../config/axios";

const resource = "profile-picture";

export default {
  upload: (data: any) =>
    secured.post(`${resource}/upload`, data, {
      headers: { "Content-Type": "multipart/form-data" },
    }),
  get: () => secured.get(`${resource}/retrieve`),
};
