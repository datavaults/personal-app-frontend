import { RiskScoreConfiguration } from "@/interfaces";
import { secured } from "../config/axios";

const resource = "risk-management";

export default {
  getPrivacyScore: (data: RiskScoreConfiguration) => secured.post(`${resource}/privacy-score`, data),
  getPiis: () => secured.get(`${resource}/piis`),
};
