import { secured } from "../config/axios";

const resource = "user";

export default {
  profile: () => secured.get(`${resource}/profile`),
  updateUser: (data: any) => secured.put(`${resource}/update`, data),
  updateLanguage: (language: string) =>
    secured.patch(`${resource}/language/${language}`),
  changeEmail: (data: any) => secured.put(`${resource}/changeemail`, data),
  delete: () => secured.delete(`user-asset/`),
  setRequestResolverSettings: (value: boolean) =>
    secured.patch(`${resource}/requestResolverSettings/${value}`),
  getEnums: (kind: string) => secured.get(`${resource}/enum/${kind}`),
  updateTerms: (demonstrator: string) => secured.patch(`${resource}/terms/${demonstrator}`),
};
