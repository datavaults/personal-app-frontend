import { secured } from "../config/axios";

const resource = "prato";
const searchResource = "search";
const certificateResource = "certificate";

export default {
    searchPersonByFiscalCode: (nationalSecurityNumber: any) => secured.post(`${resource}/${searchResource}`, nationalSecurityNumber),
    certificateFetch: (nationalSecurityNumber: any, certNumber: any) => secured.post(`${resource}/${certificateResource}`, {nationalSecurityNumber, certNumber})
};
