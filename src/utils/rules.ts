import i18n from "../main";
import { defineRule } from "vee-validate";
import { required, email, confirmed, min, max, min_value, max_value } from "@vee-validate/rules";
import { passwordStrength } from "check-password-strength";
import { PasswordStrength } from "@/constants";

defineRule("required", required);
defineRule("email", email);
defineRule("confirmed", confirmed);
defineRule("min", min);
defineRule("max", max);
defineRule("min_value", min_value);
defineRule("max_value", max_value);
defineRule("min_length", (value: any, [min]: number[]) => {
  if (value.length < min) {
    return i18n.global.t("rules.optionsMin", { number: min });
  }
  return true;
});
defineRule("max_length", (value: any, [max]: number[]) => {
  if (value.length > max) {
    return i18n.global.t("rules.optionsMax", { number: max });
  }
  return true;
});
defineRule("password", (value: string) => {
  const { contains: passwordCharacters } = passwordStrength(value, PasswordStrength.rules() as any);
  const chars: any = PasswordStrength.characters;
  for (let i = 0; i < chars.length; i += 1) {
    if (!passwordCharacters.includes(chars[i][0])) {
      return i18n.global.t("rules.passwordChar", { char: chars[i][1] });
    }
  }
  if (value.length < 8) {
    return i18n.global.t("rules.passwordMin");
  }
  return true;
});
