import { createStore } from "vuex";

interface State {
  user: any;
  sharingConfigurationUuid: string | null;
  sharingConfiguration: any;
  upToStep: number | null;
  isReady: boolean;
  auth: { token: string | null; refreshToken: string | null };
}

const store = createStore({
  state: {
    user: null,
    sharingConfigurationUuid: null,
    sharingConfiguration: {},
    upToStep: null,
    isReady: false,
    auth: {
      token: null,
      refreshToken: null,
    },
  },
  mutations: {
    SET_IS_READY(state: State) {
      state.isReady = true;
    },
    SET_USER(state: State, newUser: any) {
      state.user = newUser;
    },
    CLEAR_USER(state: State) {
      state.user = null;
    },
    ADD_CONFIGURATION(state: State, newConfiguration: { uuid: string; upToStep: number; configuration: any }) {
      const { uuid, upToStep, configuration } = newConfiguration;
      state.sharingConfigurationUuid = uuid;
      state.upToStep = upToStep;
      state.sharingConfiguration = configuration;
    },
    UPDATE_CONFIGURATION(
      state: State,
      configurationUpdate: {
        uuid: string;
        upToStep: number;
        configuration: any;
      }
    ) {
      const { uuid, upToStep, configuration } = configurationUpdate;
      if (uuid === state.sharingConfigurationUuid) {
        state.sharingConfiguration = configuration;
        state.upToStep = upToStep;
      } else {
        throw Error(`Configuration '${uuid}' is not the configuration currently in edit`);
      }
    },
    CLEAR_KEYCLOAK_TOKEN(state: State) {
      state.auth.token = null;
    },
    SET_KEYCLOAK_TOKEN(state: State, token: any) {
      state.auth.token = token;
    },
    SET_KEYCLOAK_REFRESH_TOKEN(state: State, refreshToken: any) {
      state.auth.refreshToken = refreshToken;
    },
  },
  getters: {
    isAuthenticated: (state: State) => !!state.user,
    hasShareProfileEnabled: (state: State) => state.user?.shareProfile,
    getConfigurationUuid: (state: State) => state.sharingConfigurationUuid,
    getConfiguration: (state: State) => state.sharingConfiguration,
    getUpToStep: (state: State) => state.upToStep,
    retrieveDemonstrator: (state: State) => state.user?.demonstrator,
    retieveTermsAcceptance: (state: State) => state.user?.terms,
    retrieveNationalInsuranceNumber: ( state: any )=> state.user?.nationalInsuranceNumber,
    lastTermsAcceptance: (state: State) => state.user?.lastTermsAcceptance,
  },
});

export default store;
