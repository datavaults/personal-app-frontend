import i18n from "./main";

const validationMessages = (context: any) => {
  const regExFieldName = context.field.replace(/([A-Z])/g, " $1").toLowerCase();
  //if a translation exists, give me that, otherwise take the field name
  //and turn it from camel case to regular lowercase with spaces
  const fieldName = i18n.global.te("fieldNames." + context.field)
    ? i18n.global.t("fieldNames." + context.field)
    : regExFieldName;

  switch (context.rule?.name) {
    case "required":
      return i18n.global.t("validation.required", { fieldName });
    case "email":
      return i18n.global.t("validation.email");
    case "confirmed":
      return i18n.global.t("validation.confirmed");
    case "max":
      return i18n.global.t("validation.max", { fieldName, param: context.rule.params[0] });
    case "min":
      return i18n.global.t("validation.min", { fieldName, param: context.rule.params[0] });
    case "max_value":
      return i18n.global.t("validation.maxValue", { fieldName, param: context.rule.params[0] });
    case "min_value":
      return i18n.global.t("validation.minValue", { fieldName, param: context.rule.params[0] });
    default:
      return i18n.global.t("validation.minValue", { fieldName });
  }
};

export default validationMessages;
