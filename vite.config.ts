import vue from "@vitejs/plugin-vue";
import { defineConfig } from "vite";
import { getAliases } from "vite-aliases";
import ViteComponents from "vite-plugin-components";
import Pages from "vite-plugin-pages";
import Layouts from "vite-plugin-vue-layouts";
import mkcert from "vite-plugin-mkcert";
import * as R from "ramda";
import vueI18n from "@intlify/vite-plugin-vue-i18n";
import path from "path";

export default defineConfig({
  plugins: [
    vue(),
    vueI18n({
      include: path.resolve(__dirname, "./src/messages/*.json"),
    }),
    Layouts(),
    Pages({
      exclude: ["**/components/*.vue"],
      extendRoute(route) {
        let extraMeta = {};

        // setting public routes
        const routeName: string = String(route.name);
        if (
          route.path === "/" ||
          ["about", "contact", "faq"].indexOf(routeName) >= 0 ||
          R.startsWith("auth-", routeName) ||
          R.startsWith("policies-", routeName)
        ) {
          extraMeta = { ...extraMeta, layout: "public", isPublic: true };
        } else if (R.startsWith("error-", routeName) || routeName === "all") {
          extraMeta = { ...extraMeta, layout: "empty", isPublic: true };
        }

        // adding route parent
        const splitPath = route.path.split("/");
        if (splitPath.length > 1) {
          extraMeta = { ...extraMeta, parent: splitPath[1] };
        }

        return { ...route, meta: { ...route.meta, ...extraMeta } };
      },
    }),
    ViteComponents(),
    mkcert(),
  ],
  server: {
    port: 8080,
    proxy: {
      "^/api": {
        target: "http://localhost:8000",
        changeOrigin: true,
      },
    },
  },
  resolve: {
    alias: [
      ...getAliases({ prefix: "@/" }),
      {
        find: "vue-i18n",
        replacement: "vue-i18n/dist/vue-i18n.runtime.esm-bundler.js",
      },
    ],
  },
});
