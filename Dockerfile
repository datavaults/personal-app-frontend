FROM node:14-alpine as bs

WORKDIR /app
COPY package*.json ./
RUN yarn
RUN find /app

COPY . .
RUN find /app
RUN yarn deploy

FROM node:14-alpine

# To handle 'not get uid/gid'
RUN npm config set unsafe-perm true

RUN yarn global add serve
WORKDIR /app
COPY --from=bs /app/dist/. ./
EXPOSE 8080
CMD [ "serve", "-l", "8080", "-s", "." ]
