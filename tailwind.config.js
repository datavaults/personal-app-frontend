// eslint-disable-next-line @typescript-eslint/no-var-requires
const colors = require("tailwindcss/colors");

module.exports = {
  mode: "jit",
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          50: 'var(--color-primary-50)',
          100: 'var(--color-primary-100)',
          200: 'var(--color-primary-200)',
          300: 'var(--color-primary-300)',
          400: 'var(--color-primary-400)',
          500: 'var(--color-primary-500)',
          600: 'var(--color-primary-600)',
          700: 'var(--color-primary-700)',
          800: 'var(--color-primary-800)',
          900: 'var(--color-primary-900)',
        },
        secondary: {
          50: 'var(--color-secondary-50)',
          100: 'var(--color-secondary-100)',
          200: 'var(--color-secondary-200)',
          300: 'var(--color-secondary-300)',
          400: 'var(--color-secondary-400)',
          500: 'var(--color-secondary-500)',
          600: 'var(--color-secondary-600)',
          700: 'var(--color-secondary-700)',
          800: 'var(--color-secondary-800)',
          900: 'var(--color-secondary-900)',
        },
        neutral: colors.coolGray,
        info: colors.sky,
        warn: colors.amber,
        error: colors.red,
        success: colors.green,
        sky: colors.sky,
        indigo: colors.indigo,
        blue: colors.blue,
        purple: colors.purple,
        green:colors.green
      },
    },
  },
  variants: {
    extend: {
      accessibility: ["responsive", "focus-within", "focus"],
      alignContent: ["responsive"],
      alignItems: ["responsive"],
      alignSelf: ["responsive"],
      animation: ["responsive"],
      appearance: ["responsive"],
      backgroundAttachment: ["responsive"],
      backgroundClip: ["responsive"],
      backgroundColor: [
        "responsive",
        "disabled",
        "dark",
        "group-hover",
        "focus-within",
        "hover",
        "focus",
      ],
      backgroundImage: ["responsive"],
      backgroundOpacity: [
        "responsive",
        "dark",
        "group-hover",
        "focus-within",
        "hover",
        "focus",
      ],
      backgroundPosition: ["responsive"],
      backgroundRepeat: ["responsive"],
      backgroundSize: ["responsive"],
      borderCollapse: ["responsive"],
      borderColor: [
        "responsive",
        "disabled",
        "dark",
        "group-hover",
        "focus-within",
        "hover",
        "focus",
      ],
      borderOpacity: [
        "responsive",
        "dark",
        "group-hover",
        "focus-within",
        "hover",
        "focus",
      ],
      borderRadius: ["responsive"],
      borderStyle: ["responsive"],
      borderWidth: ["responsive"],
      boxShadow: [
        "responsive",
        "disabled",
        "group-hover",
        "focus-within",
        "hover",
        "focus",
      ],
      boxSizing: ["responsive"],
      clear: ["responsive"],
      container: ["responsive"],
      cursor: ["responsive", "disabled"],
      display: ["responsive", "disabled"],
      divideColor: ["responsive", "dark"],
      divideOpacity: ["responsive", "dark"],
      divideStyle: ["responsive"],
      divideWidth: ["responsive"],
      fill: ["responsive"],
      flex: ["responsive"],
      flexDirection: ["responsive"],
      flexGrow: ["responsive"],
      flexShrink: ["responsive"],
      flexWrap: ["responsive"],
      float: ["responsive"],
      fontFamily: ["responsive"],
      fontSize: ["responsive"],
      fontSmoothing: ["responsive"],
      fontStyle: ["responsive"],
      fontVariantNumeric: ["responsive"],
      fontWeight: ["responsive"],
      gap: ["responsive"],
      gradientColorStops: ["responsive", "dark", "hover", "focus"],
      gridAutoColumns: ["responsive"],
      gridAutoFlow: ["responsive"],
      gridAutoRows: ["responsive"],
      gridColumn: ["responsive"],
      gridColumnEnd: ["responsive"],
      gridColumnStart: ["responsive"],
      gridRow: ["responsive"],
      gridRowEnd: ["responsive"],
      gridRowStart: ["responsive"],
      gridTemplateColumns: ["responsive"],
      gridTemplateRows: ["responsive"],
      height: ["responsive"],
      inset: ["responsive"],
      justifyContent: ["responsive"],
      justifyItems: ["responsive"],
      justifySelf: ["responsive"],
      letterSpacing: ["responsive"],
      lineHeight: ["responsive"],
      listStylePosition: ["responsive"],
      listStyleType: ["responsive"],
      margin: ["responsive"],
      maxHeight: ["responsive"],
      maxWidth: ["responsive"],
      minHeight: ["responsive"],
      minWidth: ["responsive"],
      objectFit: ["responsive"],
      objectPosition: ["responsive"],
      opacity: [
        "responsive",
        "disabled",
        "group-hover",
        "focus-within",
        "hover",
        "focus",
      ],
      order: ["responsive"],
      outline: ["responsive", "disabled", "focus-within", "focus"],
      overflow: ["responsive"],
      overscrollBehavior: ["responsive"],
      padding: ["responsive"],
      placeContent: ["responsive"],
      placeItems: ["responsive"],
      placeSelf: ["responsive"],
      placeholderColor: ["responsive", "dark", "focus"],
      placeholderOpacity: ["responsive", "dark", "focus"],
      pointerEvents: ["responsive"],
      position: ["responsive"],
      resize: ["responsive"],
      ringColor: ["responsive", "dark", "focus-within", "focus"],
      ringOffsetColor: ["responsive", "dark", "focus-within", "focus"],
      ringOffsetWidth: ["responsive", "focus-within", "focus"],
      ringOpacity: ["responsive", "dark", "focus-within", "focus"],
      ringWidth: ["responsive", "focus-within", "focus"],
      rotate: ["responsive", "hover", "focus"],
      scale: ["responsive", "hover", "focus"],
      skew: ["responsive", "hover", "focus"],
      space: ["responsive", "disabled"],
      stroke: ["responsive"],
      strokeWidth: ["responsive"],
      tableLayout: ["responsive"],
      textAlign: ["responsive"],
      textColor: [
        "responsive",
        "disabled",
        "dark",
        "group-hover",
        "focus-within",
        "hover",
        "focus",
      ],
      textDecoration: [
        "responsive",
        "group-hover",
        "focus-within",
        "hover",
        "focus",
      ],
      textOpacity: [
        "responsive",
        "dark",
        "group-hover",
        "focus-within",
        "hover",
        "focus",
      ],
      textOverflow: ["responsive", "disabled"],
      textTransform: ["responsive"],
      transform: ["responsive"],
      transformOrigin: ["responsive"],
      transitionDelay: ["responsive"],
      transitionDuration: ["responsive"],
      transitionProperty: ["responsive"],
      transitionTimingFunction: ["responsive"],
      translate: ["responsive", "hover", "focus"],
      userSelect: ["responsive"],
      verticalAlign: ["responsive"],
      visibility: ["responsive"],
      whitespace: ["responsive"],
      width: ["responsive"],
      wordBreak: ["responsive"],
      zIndex: ["responsive", "disabled", "focus-within", "focus"],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@mertasan/tailwindcss-variables')
  ],
};
