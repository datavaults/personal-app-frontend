#!/bin/sh

export JSON_STRING='window.configs = { \
  "VITE_FACEBOOK_APP_ID":"'"${VITE_FACEBOOK_APP_ID}"'", \
  "VITE_BACKEND_URL":"'"${VITE_BACKEND_URL}"'", \
  "VITE_CLOUD_URL":"'"${VITE_CLOUD_URL}"'", \
  "VITE_NAMESPACE":"'"${VITE_NAMESPACE}"'", \
  "VITE_REGISTRATION_OPEN":"'"${VITE_REGISTRATION_OPEN}"'", \
  "VITE_KEYCLOAK_URL":"'"${VITE_KEYCLOAK_URL}"'", \
  "VITE_KEYCLOAK_REALM":"'"${VITE_KEYCLOAK_REALM}"'", \
  "VITE_KEYCLOAK_CLIENT_ID":"'"${VITE_KEYCLOAK_CLIENT_ID}"'", \
  "VITE_ANDAMAN7_URL":"'"${VITE_ANDAMAN7_URL}"'", \
  "VITE_ANDAMAN7_API_KEY":"'"${VITE_ANDAMAN7_API_KEY}"'", \
  "VITE_STARTER_KIT_DEMO":"'"${VITE_STARTER_KIT_DEMO}"'" \
}' 
sed -i "s@<script>window.configs = {}</script>@<script>${JSON_STRING}</script>@" /usr/share/nginx/html/index.html

/docker-entrypoint.sh "$@"