<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true displayMessage=!messagesPerField.existsError('username'); section>
    <#if section = "header">
    <#elseif section = "form">
        <form id="kc-reset-password-form" class="${properties.kcFormClass!} bg-neutral-100" action="${url.loginAction}" method="post">
            <div class="bg-secondary-600">
                <div class="px-4 py-12 mx-auto max-w-7xl sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center">
                    <div class="lg:w-0 lg:flex-1">
                    <h2 class="text-3xl font-extrabold tracking-tight text-white sm:text-4xl" id="newsletter-headline">
                        ${msg("doForgotPassword")}
                    </h2>
                    <p class="max-w-3xl mt-3 text-lg leading-6 text-neutral-300">
                        ${msg("emailInstruction")} ${msg("orGoBackToLogin")} <a href="${url.loginUrl}"> ${msg("loginPage")}</a>.
                    </p>
                    </div>
                    <div class="mt-8 lg:mt-0 lg:ml-8">
                    <div class="sm:flex">
                        <div class="${properties.kcInputWrapperClass!}">
                            <#if auth?has_content && auth.showUsername()>
                                <input type="text" placeholder="<#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>" id="username" name="username" class="w-full px-5 py-3 border border-transparent rounded-md placeholder-neutral-500 focus:ring-2 focus:ring-offset-2 focus:ring-offset-secondary-800 focus:ring-white focus:border-white sm:max-w-xs" autofocus value="${auth.attemptedUsername}" aria-invalid="<#if messagesPerField.existsError('username')>true</#if>"/>
                            <#else>
                                <input placeholder="<#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>" type="text" id="username" name="username" class="w-full px-5 py-3 border border-transparent rounded-md placeholder-neutral-500 focus:ring-2 focus:ring-offset-2 focus:ring-offset-secondary-800 focus:ring-white focus:border-white sm:max-w-xs" autofocus aria-invalid="<#if messagesPerField.existsError('username')>true</#if>"/>
                            </#if>

                            <#if messagesPerField.existsError('username')>
                                <span id="input-error-username" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                            ${kcSanitize(messagesPerField.get('username'))?no_esc}
                                </span>
                            </#if>
                        </div>
                        <div class="mt-3 rounded-md shadow sm:mt-0 sm:ml-3 sm:flex-shrink-0">
                        <button type="submit" class="flex items-center justify-center w-full px-5 py-3 text-base font-medium text-white border border-transparent rounded-md bg-secondary-800 hover:bg-secondary-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-neutral-800 focus:ring-secondary-500">
                            ${msg("resetPasswordButton")}
                        </button>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

        </form>
    <#elseif section = "info" >
    </#if>
</@layout.registrationLayout>
