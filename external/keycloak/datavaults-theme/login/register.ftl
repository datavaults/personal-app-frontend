<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('firstName','lastName','email','username','password','password-confirm'); section>
    <#if section = "header">
    <#elseif section = "form">
        <div class="flex flex-col justify-center flex-1 px-6 py-12 sm:px-8 bg-neutral-100">
            <div class="sm:mx-auto sm:w-full sm:max-w-md">
                <h2 class="text-2xl font-extrabold text-center text-primary-800 sm:text-3xl">${msg("registerTitle")}</h2>
            </div>
            <form id="kc-register-form" class="flex flex-col flex-1" action="${url.registrationAction}" method="post">
                <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
                    <div class="flex flex-col px-4 py-8 space-y-6 bg-white shadow sm:rounded-lg sm:px-10">
                        <div class="space-y-4">
                            <!---->
                            <div rules="required|email">
                            <div class="flex flex-col space-y-1">
                                <label for="email" class="flex items-center text-sm font-medium tracking-wide text-neutral-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4 mr-1">
                                        <path fill-rule="evenodd" d="M14.243 5.757a6 6 0 10-.986 9.284 1 1 0 111.087 1.678A8 8 0 1118 10a3 3 0 01-4.8 2.401A4 4 0 1114 10a1 1 0 102 0c0-1.537-.586-3.07-1.757-4.243zM12 10a2 2 0 10-4 0 2 2 0 004 0z" clip-rule="evenodd"></path>
                                    </svg>
                                    ${msg("email")}
                                </label>
                                <div>
                                    <input type="text" id="email" class="block w-full px-3 py-2 border rounded-md shadow-sm appearance-none placeholder-neutral-400 border-neutral-300 focus:outline-none focus:ring-primary-500 focus:border-primary-500 sm:text-sm" name="email"
                                        value="${(register.formData.email!'')}" autocomplete="email"
                                        aria-invalid="<#if messagesPerField.existsError('email')>true</#if>"
                                    />

                                    <#if messagesPerField.existsError('email')>
                                        <span id="input-error-email" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                            ${kcSanitize(messagesPerField.get('email'))?no_esc}
                                        </span>
                                    </#if>
                                </div>
                                <!---->
                            </div>
                            </div>
                            <div rules="required">
                            <div class="flex flex-col space-y-1">
                                <label for="password" class="flex items-center text-sm font-medium tracking-wide text-neutral-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="w-4 h-4 mr-1">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path>
                                    </svg>
                                    ${msg("password")}
                                </label>
                                <div>
                                    <input type="password" id="password" class="block w-full px-3 py-2 border rounded-md shadow-sm appearance-none placeholder-neutral-400 border-neutral-300 focus:outline-none focus:ring-primary-500 focus:border-primary-500 sm:text-sm" name="password"
                                        autocomplete="new-password"
                                        aria-invalid="<#if messagesPerField.existsError('password','password-confirm')>true</#if>"
                                    />

                                    <#if messagesPerField.existsError('password')>
                                        <span id="input-error-password" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                            ${kcSanitize(messagesPerField.get('password'))?no_esc}
                                        </span>
                                    </#if>
                                </div>
                                <!---->
                            </div>
                            </div>
                            <div rules="required">
                            <div class="flex flex-col space-y-1">
                                <label for="confirmPassword" class="flex items-center text-sm font-medium tracking-wide text-neutral-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="w-4 h-4 mr-1">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path>
                                    </svg>
                                    ${msg("passwordConfirm")}
                                </label>
                                <div>
                                    <input type="password" id="password-confirm" class="block w-full px-3 py-2 border rounded-md shadow-sm appearance-none placeholder-neutral-400 border-neutral-300 focus:outline-none focus:ring-primary-500 focus:border-primary-500 sm:text-sm"
                                        name="password-confirm"
                                        aria-invalid="<#if messagesPerField.existsError('password-confirm')>true</#if>"
                                    />

                                    <#if messagesPerField.existsError('password-confirm')>
                                        <span id="input-error-password-confirm" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                            ${kcSanitize(messagesPerField.get('password-confirm'))?no_esc}
                                        </span>
                                    </#if>
                                </div>
                                <!---->
                            </div>
                            </div>
                            <div class="space-y-3">
                            <div>
                                <input class="flex justify-center w-full px-4 py-2 text-sm font-medium tracking-wide text-white uppercase border border-transparent rounded-md shadow-sm bg-primary-600 hover:bg-primary-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:bg-primary-300 disabled:cursor-not-allowed" type="submit" value="${msg('doRegister')}"/>
                            </div>
                        </div>
                        <#if realm.password && social.providers??>
                            <div>
                                <div class="relative">
                                <div class="absolute inset-0 flex items-center">
                                    <div class="w-full border-t border-neutral-300"></div>
                                </div>
                                <div class="relative flex justify-center text-sm"><span class="px-2 bg-white text-neutral-500"> Or sign up with </span></div>
                                </div>
                                    <div id="kc-social-providers" class="grid justify-center grid-rows-1 gap-3 mt-6">
                                        <#list social.providers as p>
                                            <#if p.displayName =="Facebook">
                                            <span class="sr-only">Login with ${p.displayName!}</span>
                                            <div class="customSocialBtn" >
                                                <a id="social-${p.alias}" href="${p.loginUrl}" class="socialLinkBtn" >
                                                <span class="social-fb-icon"></span>
                                                <span class="facebookButtonText">Login with Facebook</span>
                                                </a>
                                            </div>
                                            </#if>
                                            <#if p.displayName =='Google'>
                                            <span class="sr-only">Sign in with ${p.displayName!}</span>
                                            <div class="customSocialBtn">
                                                <a id="social-google" href="${p.loginUrl}" class="socialLinkBtn" >
                                                <span class="g-icon"></span>
                                                <span class="googleButtonText">Sign in with Google</span>
                                                </a>
                                            </div>
                                            </#if>
                                            <#if p.displayName =='Twitter'>
                                            <span class="sr-only">Sign in with ${p.displayName!}</span>
                                            <a id="social-twitter" href="${p.loginUrl}" class="socialLinkBtn" ><div id="customTwitterBtn" > </div></a>
                                            </#if>
                                        </#list>
                                    </div>
                            </div>
                        </#if>
                        <div class="flex flex-row justify-end text-neutral-700"><a href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a></div>

                    </div>
                </div>
            </form>
        </div>
    <#elseif section = "info" >
    </#if>


</@layout.registrationLayout>