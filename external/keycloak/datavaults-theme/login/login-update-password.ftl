<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('password','password-confirm'); section>
    <#if section = "header">
    <#elseif section = "form">
        <form id="kc-passwd-update-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
           <div class="flex flex-col justify-center min-h-full py-12 sm:px-6 lg:px-8">
                <div class="sm:mx-auto sm:w-full sm:max-w-md">
                    <h2 class="mt-6 text-3xl font-extrabold text-center text-neutral-900">
                        ${msg("updatePasswordTitle")}
                    </h2>
                </div>

                <input type="text" id="username" name="username" value="${username}" autocomplete="username"
                    readonly="readonly" style="display:none;"/>
                <input type="password" id="password" name="password" autocomplete="current-password" style="display:none;"/>

                <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
                    <div class="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10">
                    <div class="space-y-6" >
                        <div>
                        <label for="email" class="block text-sm font-medium text-neutral-700">
                            ${msg("passwordNew")}
                        </label>
                        <div class="mt-1">
                            <input type="password" id="password-new" name="password-new" class="block w-full px-3 py-2 border rounded-md shadow-sm appearance-none placeholder-neutral-400 border-neutral-300 focus:outline-none focus:ring-secondary-500 focus:border-secondary-500 sm:text-sm"
                                autofocus autocomplete="new-password"
                                aria-invalid="<#if messagesPerField.existsError('password','password-confirm')>true</#if>"
                            />
                             <#if messagesPerField.existsError('password')>
                                    <span id="input-error-password" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                    ${kcSanitize(messagesPerField.get('password'))?no_esc}
                                </span>
                            </#if>
                        </div>
                        </div>

                        <div>
                        <label for="password" class="block text-sm font-medium text-neutral-700">
                            ${msg("passwordConfirm")}
                        </label>
                        <div class="mt-1">
                            <input type="password" id="password-confirm" name="password-confirm"
                                class="block w-full px-3 py-2 border rounded-md shadow-sm appearance-none placeholder-neutral-400 border-neutral-300 focus:outline-none focus:ring-secondary-500 focus:border-secondary-500 sm:text-sm"
                                autocomplete="new-password"
                                aria-invalid="<#if messagesPerField.existsError('password-confirm')>true</#if>"
                            />

                            <#if messagesPerField.existsError('password-confirm')>
                                <span id="input-error-password-confirm" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                    ${kcSanitize(messagesPerField.get('password-confirm'))?no_esc}
                                </span>
                            </#if>
                        </div>
                        </div>

                        <div>
                        <input class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white border border-transparent rounded-md shadow-sm bg-secondary-600 hover:bg-secondary-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-secondary-500" ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doSubmit")}" />

                        </div>
                    </div>
                </div>
            </div>

        </form>
    </#if>
</@layout.registrationLayout>