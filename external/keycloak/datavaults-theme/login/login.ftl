<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('username','password') displayInfo=realm.password && realm.registrationAllowed && !registrationDisabled??; section>
    <#if section = "header">
    <#elseif section = "form">
    <div >
      <div >
        <#if realm.password>
            <form onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post" class="flex flex-col justify-center flex-1 px-6 py-12 sm:px-8 bg-neutral-100" >
                <div class="sm:mx-auto sm:w-full sm:max-w-md">
                  <h2 class="text-2xl font-extrabold text-center text-primary-800 sm:text-3xl">
                   ${msg("loginAccountTitle")}
                   </h2>
                </div>

                <div class="mt-8 sm:mx-auto sm:w-full md:max-w-lg">

            <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
              <div class="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10">
                <div class="space-y-6">
                  <!---->
                  <div rules="required|email">
                    <div class="flex flex-col space-y-1">
                      <label for="email" class="flex items-center text-sm font-medium tracking-wide text-neutral-600">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4 mr-1">
                          <path fill-rule="evenodd" d="M14.243 5.757a6 6 0 10-.986 9.284 1 1 0 111.087 1.678A8 8 0 1118 10a3 3 0 01-4.8 2.401A4 4 0 1114 10a1 1 0 102 0c0-1.537-.586-3.07-1.757-4.243zM12 10a2 2 0 10-4 0 2 2 0 004 0z" clip-rule="evenodd"></path></svg>
                          ${msg("email")}
                      </label>
                    <div>
                   <input tabindex="1" id="username"class="block w-full px-3 py-2 border rounded-md shadow-sm appearance-none placeholder-neutral-400 border-neutral-300 focus:outline-none focus:ring-primary-500 focus:border-primary-500 sm:text-sm" name="username" value="${(login.username!'')}"  type="text" autofocus autocomplete="off"
                               aria-invalid="<#if messagesPerField.existsError('username','password')>true</#if>"
                        />
                    <#if messagesPerField.existsError('username','password')>
                        <span id="input-error" class="text-red-600" aria-live="polite">
                                ${kcSanitize(messagesPerField.getFirstError('username','password'))?no_esc}
                        </span>
                    </#if>
                </div>
                <!---->
              </div>
            </div>
            <div rules="required">
              <div class="flex flex-col space-y-1">
                <label for="password" class="flex items-center text-sm font-medium tracking-wide text-neutral-600">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="w-4 h-4 mr-1"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path></svg>
                  ${msg("password")}
                </label>
              <div>
              <input tabindex="2" id="password" class="block w-full px-3 py-2 border rounded-md shadow-sm appearance-none placeholder-neutral-400 border-neutral-300 focus:outline-none focus:ring-primary-500 focus:border-primary-500 sm:text-sm" placeholder="" name="password" type="password" autocomplete="off"
                                    aria-invalid="<#if messagesPerField.existsError('username','password')>true</#if>"
                            />
            </div>
            <!---->
          </div>
        </div>
        <div class="flex items-center justify-center">
        <div class="text-sm">
          <a href="${url.registrationUrl}" class="font-medium text-primary-600 hover:text-primary-500">${msg("noAccount")}</a>
        </div>
        <span class="mx-2 font-bold text-md text-secondary-300">♦</span>
          <#if realm.resetPasswordAllowed>
            <div class="text-sm">
              <a href="${url.loginResetCredentialsUrl}" class="font-medium text-primary-600 hover:text-primary-500">${msg("doForgotPassword")}</a>
            </div>
          </#if>
          </div>
          <div>
            <input tabindex="4" class="flex justify-center w-full px-4 py-2 text-sm font-medium tracking-wide text-white uppercase border border-transparent rounded-md shadow-sm bg-primary-600 hover:bg-primary-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:cursor-not-allowed disabled:opacity-30" name="login" id="kc-login" type="submit" value="${msg("doLogIn")}"/>
          </div>
        </div>
        <div class="mt-6">
          <div class="relative">
          <div class="absolute inset-0 flex items-center">
          <div class="w-full border-t border-neutral-300"></div>
        </div>
        <div class="relative flex justify-center text-sm">
          <span class="px-2 bg-white text-neutral-500"> Or continue with </span>
        </div>
      </div>
      <#if realm.password && social.providers??>
            <div id="kc-social-providers" class="grid justify-center grid-rows-1 gap-3 mt-6">
                <#list social.providers as p>
                    <#if p.displayName =="Facebook">
                    <span class="sr-only">Login with ${p.displayName!}</span>
                      <div class="customSocialBtn" >
                        <a id="social-${p.alias}" href="${p.loginUrl}" class="socialLinkBtn" >
                          <span class="social-fb-icon"></span>
                          <span class="facebookButtonText">Login with Facebook</span>
                        </a>
                      </div>
                    </#if>
                    <#if p.displayName =='Google'>
                    <span class="sr-only">Sign in with ${p.displayName!}</span>
                      <div class="customSocialBtn">
                        <a id="social-google" href="${p.loginUrl}" class="socialLinkBtn" >
                          <span class="g-icon"></span>
                          <span class="googleButtonText">Sign in with Google</span>
                        </a>
                      </div>
                    </#if>
                    <#if p.displayName =='Twitter'>
                    <span class="sr-only">Sign in with ${p.displayName!}</span>
                      <a id="social-twitter" href="${p.loginUrl}" class="socialLinkBtn" ><div id="customTwitterBtn" > </div></a>
                    </#if>
                </#list>
            </div>
        </#if>
    </div>
  </div>
</div>


            </form>
        </#if>
        </div>


    </div>
    <#elseif section = "info" >

    </#if>

</@layout.registrationLayout>
