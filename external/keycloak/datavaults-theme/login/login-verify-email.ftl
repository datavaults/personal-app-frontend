<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "header">
    <#elseif section = "form">
    <div class="py-16 sm:py-24">
  <div class="relative sm:py-16">
    <div aria-hidden="true" class="hidden sm:block">
      <div class="absolute inset-y-0 left-0 w-1/2 bg-neutral-50 rounded-r-3xl"></div>
      <svg class="absolute -ml-3 top-8 left-1/2" width="404" height="392" fill="none" viewBox="0 0 404 392">
        <defs>
          <pattern id="8228f071-bcee-4ec8-905a-2a059a2cc4fb" x="0" y="0" width="20" height="20" patternUnits="userSpaceOnUse">
            <rect x="0" y="0" width="4" height="4" class="text-neutral-200" fill="currentColor" />
          </pattern>
        </defs>
        <rect width="404" height="392" fill="url(#8228f071-bcee-4ec8-905a-2a059a2cc4fb)" />
      </svg>
    </div>
    <div class="max-w-md px-4 mx-auto sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
      <div class="relative px-6 py-10 overflow-hidden shadow-xl bg-primary-600 rounded-2xl sm:px-12 sm:py-20">
        <div aria-hidden="true" class="absolute inset-0 -mt-72 sm:-mt-32 md:mt-0">
          <svg class="absolute inset-0 w-full h-full" preserveAspectRatio="xMidYMid slice" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 1463 360">
            <path class="text-primary-500 text-opacity-40" fill="currentColor" d="M-82.673 72l1761.849 472.086-134.327 501.315-1761.85-472.086z" />
            <path class="text-primary-700 text-opacity-40" fill="currentColor" d="M-217.088 544.086L1544.761 72l134.327 501.316-1761.849 472.086z" />
          </svg>
        </div>
        <div class="relative">
          <div class="sm:text-center">
            <h2 class="text-3xl font-extrabold tracking-tight text-white sm:text-4xl">
              ${msg("emailVerifyInstruction1")}
            </h2>
            <p class="max-w-2xl mx-auto mt-6 text-lg text-primary-200">
            ${msg("emailVerifyInstruction2")}
            </p>
          </div>
          <div class="justify-center mt-12 sm:mx-auto sm:max-w-lg sm:flex">
            <div class="mt-4 sm:mt-0 sm:ml-3">
              <a href="${url.loginAction}" class="block w-full px-5 py-3 text-base font-medium text-white border border-transparent rounded-md shadow bg-primary-500 hover:bg-primary-400 hover:text-neutral-100 focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-primary-600 sm:px-10">${msg("emailVerifyInstruction3")}</a>
            </div>

          </div>
            <div class="justify-center mt-6 sm:mx-auto sm:max-w-lg sm:flex">
            <div class="mt-4 text-neutral-200 sm:mt-0 sm:ml-3">
                <a href="${url.loginRestartFlowUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
    <#elseif section = "info">

    </#if>
</@layout.registrationLayout>